<?php
  $descripcion = CFS()->get( 'descripcion', get_the_ID(), array( 'format' => 'api' ));
  $slug = get_post_field( 'post_name', get_post() );
  $imagenes = CFS()->get( 'imagenes_elemento', get_the_ID(), array( 'format' => 'api' ));

?>

<section id="<?php echo $slug; ?>" class="bg-black-222 py-5" style="">
  <div class="container py-5">
    <div class="section-title">
		<div class="row">
			<div class="col">
				<h5 class="sub-title text-gray mb-0">Servicios</h5>
				<h2 class="title mt-0"><?php the_title(); ?></h2>
			</div>
		</div>
	</div>
	<div class="section-content">
      <div class="row g-3">
        <div class="col-md-8 col-12">
			<?php echo $descripcion; ?>
		</div>
		
		
		<div class="col-md-4 col-12">
			<div class="tm-owl-carousel-1col">
				<?php foreach($imagenes as $imagen){ ?>
					<div class="item">
						<img src="<?php echo $imagen['imagen_elemento']; ?>" class="owl-height" alt="">
					</div>
				<?php } ?>
			</div>
		</div>
      </div>
    </div>
  </div>
</section>


