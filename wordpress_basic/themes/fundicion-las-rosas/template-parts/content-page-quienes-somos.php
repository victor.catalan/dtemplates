<?php
	$my_wp_query = new WP_Query();
	$all_wp_pages = $my_wp_query->query(array('post_type' => 'page'));
	$children = get_page_children( $post->ID, $all_wp_pages );
?>

<div id="post-<?php the_ID(); ?>" class="main-content-area">
	<section class="about-section2">
      <div class="container pb-140 pb-lg-100">
		<?php the_content(); ?>
	  </div>
	</section>
</div>

<?php foreach ($children as $page) { ?>
	<div class="separator separator-rouned2">
		<i class="fa fa-cog fa-spin2"></i>
	</div>
	<div id="post-<?php ?>" class="main-content-area">
		<section class="about-section2">
			<div class="container pb-140 pb-lg-100">
				<div class="section-title">
					<div class="row">
						<div class="col">
							<h5 class="sub-title text-gray mb-0">Quienes Somos</h5>
							<h2 class="title mt-0"><?php echo($page->post_title); ?></h2>
						</div>
					</div>
				</div>
				<div class="section-content">
					<div class="row">
						<div class="col-12">
							<?php echo($page->post_content); ?>
						</div>
					</div>
				</div>
		
		</div>
		</section>
	</div>
<?php } ?>
