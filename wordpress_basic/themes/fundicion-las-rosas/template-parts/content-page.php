<div id="post-<?php the_ID(); ?>" class="main-content-area">
	<?php include get_theme_file_path( 'template-parts/page-tile.php' );  ?>
	<section class="about-section2">
      <div class="container pb-140 pb-lg-100">
		<?php the_content(); ?>
	  </div>
	</section>
</div>

