<?php
  $descripcion = CFS()->get( 'descripcion', get_the_ID(), array( 'format' => 'api' ));
  $slug = get_post_field( 'post_name', get_post() );
  $imagenes = CFS()->get( 'imagenes_elemento', get_the_ID(), array( 'format' => 'api' ));
?>
<section id="producto-<?php the_ID(); ?>" class="bg-black-222">
  <div class="container py-5">
    <div class="section-title">
      <div class="row">
        <div class="col-md-8">
          <h5 class="sub-title text-gray mb-0">Sub Title Here</h5>
          <h2 class="title mt-0">Title Style One</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo et adipisci, beatae ad sapiente aspernatur accusamus!</p>
        </div>
		<div class="col-md-4">
			<div class="tm-owl-carousel-1col">
				<div class="item"><img src="http://placehold.it/400x400" alt=""></div>
				<div class="item"><img src="http://placehold.it/400x400" alt=""></div>
				<div class="item"><img src="http://placehold.it/400x400" alt=""></div>
			</div>
		</div>
      </div>
    </div>
  </div>
</section>

