<?php get_header(); ?>
<?php
  $page_productos = get_page_by_title('Productos');//get_page_by_path('/home', OBJECT, 'page');
  $subtitulo_aleaciones = CFS()->get( 'subtitulos_aleaciones', $page_productos->ID, array( 'format' => 'api' ));
  $subtitulo_rubros = CFS()->get( 'subtitulos_rubros', $page_productos->ID, array( 'format' => 'api' ));
  $subtitulo_categorias = CFS()->get( 'subtitulos_categorias', $page_productos->ID, array( 'format' => 'api' ));

  $texto_archivo_pdf = CFS()->get( 'texto_archivo_pdf', $page_productos->ID, array( 'format' => 'api' ));
  $texto_archivo_excel = CFS()->get( 'texto_archivo_excel', $page_productos->ID, array( 'format' => 'api' ));

  $archivo_pdf = CFS()->get( 'archivo_pdf', $page_productos->ID, array( 'format' => 'api' ));
  $archivo_excel = CFS()->get( 'archivo_excel', $page_productos->ID, array( 'format' => 'api' ));

  $imagenes_aleaciones = CFS()->get( 'imagenes-productos-aleaciones', $page_productos->ID, array( 'format' => 'api' ));
  $imagenes_rubros = CFS()->get( 'imagenes-productos-rubros', $page_productos->ID, array( 'format' => 'api' ));
  $imagenes_categorias = CFS()->get( 'imagenes-productos-categorias', $page_productos->ID, array( 'format' => 'api' ));
  
?>
<?php include get_theme_file_path( 'template-parts/page-tile.php' );  ?>


<section id="rubros" class="bg-black-222 py-5" style="">
  <div class="container py-5">
    <div class="section-title">
		<div class="row">
			<div class="col">
				<h5 class="sub-title text-gray mb-0">Productos</h5>
				<h2 class="title mt-0 position-relative">
					Rubros
					<?php if($texto_archivo_pdf != ''){ ?>
					<div class="position-absolute top-0 end-0">
						<a class="fs-6 d-block fw-normal" target="_blank" href="<?php echo $archivo_pdf; ?>">
							<i class="fa fa-file-pdf"></i>
							<?php echo $texto_archivo_pdf; ?>
						</a>
					</div>
					<?php } ?>
				</h2>
				<p>
					<?php echo $subtitulo_rubros; ?>	
				</p>	
			</div>
		</div>
	</div>
	<div class="section-content">
      <div class="row g-3">
        <div class="col-md-8 col-12">

			<div class="accordion accordion-flush2" id="accordion-rubros">
				<?php
				$rubros = get_terms( array(
					'taxonomy' => 'rubros',
					'hide_empty' => false,
				) );
				$i=1;
				?>
				<?php foreach ( $rubros as $rubro ) : ?>
				<div class="accordion-item">
					<h2 class="accordion-header" id="flush-heading-<?php echo($rubro->slug); ?>">
						<button class="accordion-button <?php echo $i==1?'':'collapsed'; ?>" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse-<?php echo($rubro->slug); ?>" aria-expanded="<?php echo $i==1?'true':'false'; ?>" aria-controls="flush-collapse-<?php echo($rubro->slug); ?>">
							<strong><?php echo($rubro->name); ?></strong>
						</button>
					</h2>
					<div id="flush-collapse-<?php echo($rubro->slug); ?>" class="accordion-collapse <?php echo $i==1?'collapse show':'collapse'; ?>" aria-labelledby="flush-heading-<?php echo($rubro->slug); ?>" data-bs-parent="#accordion-rubros" style="">
						<div class="accordion-body">
							<?php echo($rubro->description); ?>
						</div>
					</div>
				</div>
				<?php $i++; ?>
				<?php endforeach; ?>
			</div>
		</div>
		
		
		<div class="col-md-4 col-12">
			<div class="tm-owl-carousel-1col">
				<?php foreach($imagenes_rubros as $imagen){ ?>
					<div class="item">
						<img src="<?php echo $imagen['imagen_galeria_productos_rubros']; ?>" class="owl-height" alt="">
					</div>
				<?php } ?>
			</div>
		</div>
      </div>
    </div>
  </div>
</section>

<div class="separator separator-rouned2">
  <i class="fa fa-cog fa-spin2"></i>
</div>

<section id="aleaciones" class="bg-black-222 py-5" style="">
  <div class="container py-5">
    <div class="section-title">
		<div class="row">
			<div class="col">
				<h5 class="sub-title text-gray mb-0">Productos</h5>
				<h2 class="title mt-0 position-relative">
					Aleaciones
					<?php if($texto_archivo_excel != ''){ ?>
					<div class="position-absolute top-0 end-0">
						<a class="fs-6 d-block fw-normal" target="_blank" href="<?php echo $archivo_excel; ?>">
							<i class="fa fa-file-excel"></i>
							<?php echo $texto_archivo_excel; ?>
						</a>
					</div>
					<?php } ?>
				</h2>
				<p><?php echo $subtitulo_aleaciones; ?>	</p>	
			</div>
		</div>
	</div>
	<div class="section-content">
      <div class="row g-3">
        <div class="col-md-8 col-12">

			<div class="accordion accordion-flush2" id="accordion-aleaciones">
				<?php
				$aleaciones = get_terms( array(
					'taxonomy' => 'aleaciones',
					'hide_empty' => false,
				) );
				$i=1;
				?>
				<?php foreach ( $aleaciones as $aleacion ) : ?>
				<div class="accordion-item">
					<h2 class="accordion-header" id="flush-heading-<?php echo($rubro->slug); ?>">
						<button class="accordion-button <?php echo $i==1?'':'collapsed'; ?>" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse-<?php echo($aleacion->slug); ?>" aria-expanded="<?php echo $i==1?'true':'false'; ?>" aria-controls="flush-collapse-<?php echo($aleacion->slug); ?>">
							<strong><?php echo($aleacion->name); ?></strong>
						</button>
					</h2>
					<div id="flush-collapse-<?php echo($aleacion->slug); ?>" class="accordion-collapse <?php echo $i==1?'collapse show':'collapse'; ?>" aria-labelledby="flush-heading-<?php echo($aleacion->slug); ?>" data-bs-parent="#accordion-aleaciones" style="">
						<div class="accordion-body">
							<?php echo($aleacion->description); ?>
						</div>
					</div>
				</div>
				<?php $i++; ?>
				<?php endforeach; ?>
			</div>
		</div>
		
		
		<div class="col-md-4 col-12">
			<div class="tm-owl-carousel-1col">
				<?php foreach($imagenes_aleaciones as $imagen){ ?>
					<div class="item">
						<img src="<?php echo $imagen['imagen_galeria_productos_aleaciones']; ?>" class="owl-height" alt="">
					</div>
				<?php } ?>
			</div>
		</div>
      </div>
    </div>
  </div>
</section>

<div class="separator separator-rouned2">
  <i class="fa fa-cog fa-spin2"></i>
</div>

<section id="categorias" class="bg-black-222 py-5" style="">
  <div class="container py-5">
    <div class="section-title">
		<div class="row">
			<div class="col">
				<h5 class="sub-title text-gray mb-0">Productos</h5>
				<h2 class="title mt-0">Categorias</h2>
				<p><?php echo $subtitulo_categorias; ?>	</p>	
			</div>
		</div>
	</div>
	<div class="section-content">
      <div class="row g-3">
        <div class="col-md-8 col-12">

			<div class="accordion accordion-flush2" id="accordion-categorias">
				<?php
				$categorias = get_terms( array(
					'taxonomy' => 'categorias-producto',
					'hide_empty' => false,
				) );
				$i=1;
				?>
				<?php foreach ( $categorias as $categoria ) : ?>
				<div class="accordion-item">
					<h2 class="accordion-header" id="flush-heading-<?php echo($categoria->slug); ?>">
						<button class="accordion-button <?php echo $i==1?'':'collapsed'; ?>" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse-<?php echo($categoria->slug); ?>" aria-expanded="<?php echo $i==1?'true':'false'; ?>" aria-controls="flush-collapse-<?php echo($categoria->slug); ?>">
							<strong><?php echo($categoria->name); ?></strong>
						</button>
					</h2>
					<div id="flush-collapse-<?php echo($categoria->slug); ?>" class="accordion-collapse <?php echo $i==1?'collapse show':'collapse'; ?>" aria-labelledby="flush-heading-<?php echo($categoria->slug); ?>" data-bs-parent="#accordion-categorias" style="">
						<div class="accordion-body">
							<?php echo($categoria->description); ?>
						</div>
					</div>
				</div>
				<?php $i++; ?>
				<?php endforeach; ?>
			</div>
		</div>
		
		
		<div class="col-md-4 col-12">
			<div class="tm-owl-carousel-1col">
				<?php foreach($imagenes_categorias as $imagen){ ?>
					<div class="item">
						<img src="<?php echo $imagen['imagen_galeria_productos_categorias']; ?>" class="owl-height" alt="">
					</div>
				<?php } ?>
			</div>
		</div>
      </div>
    </div>
  </div>
</section>






<?php while ( have_posts() ) :
	the_post();
	//get_template_part( 'template-parts/content-page-productos' );  
endwhile; ?>

<?php get_footer(); ?>
