<?php 

//Set auto:
// activar theme
// activar plugins
// conf permalinks
// set tagline
// create home set as homepage




function create_rubros_hierarchical_taxonomy() { 
    $labels = array(
        'name' => _x( 'Clientes', 'taxonomy general name' ),
        'singular_name' => _x( 'Cliente', 'taxonomy singular name' ),
        'search_items' =>  __( 'Buscar Clientes' ),
        'all_items' => __( 'Todos los Clientes' ),
        'parent_item' => __( 'Padre Cliente' ),
        'parent_item_colon' => __( 'Padre Cliente:' ),
        'edit_item' => __( 'Editar Cliente' ), 
        'update_item' => __( 'Actualizar Cliente' ),
        'add_new_item' => __( 'Agregar nuevo Cliente' ),
        'new_item_name' => __( 'Nombre Nuevo Cliente' ),
        'menu_name' => __( 'Clientes' ),
    );    
    register_taxonomy('clientes',array('home'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_in_rest' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'clientes' ),
    ));

    $labels = array(
        'name' => _x( 'Rubros', 'taxonomy general name' ),
        'singular_name' => _x( 'Rubro', 'taxonomy singular name' ),
        'search_items' =>  __( 'Buscar Rubros' ),
        'all_items' => __( 'Todos los Rubros' ),
        'parent_item' => __( 'Padre Rubro' ),
        'parent_item_colon' => __( 'Padre Rubro:' ),
        'edit_item' => __( 'Editar Rubro' ), 
        'update_item' => __( 'Actualizar Rubro' ),
        'add_new_item' => __( 'Agregar nuevo Rubro' ),
        'new_item_name' => __( 'Nombre Nuevo Rubro' ),
        'menu_name' => __( 'Rubros' ),
    );    
    register_taxonomy('rubros',array('productos'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_in_rest' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'rubros' ),
    ));

    $labels = array(
        'name' => _x( 'Aleaciones', 'taxonomy general name' ),
        'singular_name' => _x( 'Aleacion', 'taxonomy singular name' ),
        'search_items' =>  __( 'Buscar Aleaciones' ),
        'all_items' => __( 'Todas las Aleaciones' ),
        'parent_item' => __( 'Padre Aleacion' ),
        'parent_item_colon' => __( 'Padre Aleacion:' ),
        'edit_item' => __( 'Editar Aleacion' ), 
        'update_item' => __( 'Actualizar Aleacion' ),
        'add_new_item' => __( 'Agregar nueva Aleacion' ),
        'new_item_name' => __( 'Nombre nueva Aleacion' ),
        'menu_name' => __( 'Aleaciones' ),
    );    
    register_taxonomy('aleaciones',array('productos'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_in_rest' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'aleacion' ),
    ));

    $labels = array(
        'name' => _x( 'Categorias Producto', 'taxonomy general name' ),
        'singular_name' => _x( 'Categoria Producto', 'taxonomy singular name' ),
        'search_items' =>  __( 'Buscar Categorias Producto' ),
        'all_items' => __( 'Todas las Categorias Producto' ),
        'parent_item' => __( 'Padre Categoria Producto' ),
        'parent_item_colon' => __( 'Padre Categoria Producto:' ),
        'edit_item' => __( 'Editar Categoria Producto' ), 
        'update_item' => __( 'Actualizar Categoria Producto' ),
        'add_new_item' => __( 'Agregar nueva Categoria Producto' ),
        'new_item_name' => __( 'Nombre nueva Categoria Producto' ),
        'menu_name' => __( 'Categorias Producto' ),
    );    
    register_taxonomy('categorias-producto',array('productos'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_in_rest' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'categorias-producto' ),
    ));

  
}

function create_posttype() {
    
    register_post_type( 'productos',
        array(
            'labels' => array(
                'name' => __( 'Productos' ),
                'singular_name' => __( 'Producto' ),
                'search_items' =>  __( 'Buscar Productos' ),
                'all_items' => __( 'Todos los Productos' ),
                'parent_item' => __( 'Padre Producto' ),
                'parent_item_colon' => __( 'Padre Producto:' ),
                'edit_item' => __( 'Editar Producto' ), 
                'update_item' => __( 'Actualizar Producto' ),
                'add_new_item' => __( 'Agregar nuevo Producto' ),
                'new_item_name' => __( 'Nombre nuevo Producto' ),
                'menu_name' => __( 'Productos' ),
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'productos'),
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => true,
            'show_in_rest' => true,

        )
    );
    register_post_type( 'servicios',
        array(
            'labels' => array(
                'name' => __( 'Servicios' ),
                'singular_name' => __( 'Servicio' ),
                'search_items' =>  __( 'Buscar Servicios' ),
                'all_items' => __( 'Todos los Servicios' ),
                'parent_item' => __( 'Padre Servicio' ),
                'parent_item_colon' => __( 'Padre Servicio:' ),
                'edit_item' => __( 'Editar Servicio' ), 
                'update_item' => __( 'Actualizar Servicio' ),
                'add_new_item' => __( 'Agregar nuevo Servicio' ),
                'new_item_name' => __( 'Nombre nuevo Servicio' ),
                'menu_name' => __( 'Servicios' ),
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'servicios'),
            'show_in_rest' => true,

        )
    );

    register_post_type( 'clientes',
        array(
            'labels' => array(
                'name' => __( 'Clientes' ),
                'singular_name' => __( 'Cliente' ),
                'search_items' =>  __( 'Buscar Clientes' ),
                'all_items' => __( 'Todos los Clientes' ),
                'parent_item' => __( 'Padre Cliente' ),
                'parent_item_colon' => __( 'Padre Cliente:' ),
                'edit_item' => __( 'Editar Cliente' ), 
                'update_item' => __( 'Actualizar Cliente' ),
                'add_new_item' => __( 'Agregar nuevo Cliente' ),
                'new_item_name' => __( 'Nombre nuevo Cliente' ),
                'menu_name' => __( 'Clientes' ),
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'clientes'),
            'show_in_rest' => true,

        )
    );
}


function remove_menus(){  

    //remove_menu_page( 'index.php' );                  //Dashboard  
    remove_menu_page( 'edit.php' );                   //Posts  
    //remove_menu_page( 'upload.php' );                 //Media  
    //remove_menu_page( 'edit.php?post_type=page' );    //Pages  
    remove_menu_page( 'edit-comments.php' );          //Comments  
    remove_menu_page( 'themes.php' );                 //Appearance  
    remove_menu_page( 'plugins.php' );                //Plugins  
    //remove_menu_page( 'users.php' );                  //Users  
    remove_menu_page( 'tools.php' );                  //Tools  
    //remove_menu_page( 'options-general.php' );        //Settings  
  
}  

function register_menus() {
    register_nav_menus(
        array(
            'header-menu' => __( 'Header Menu' ),
            'footer-menu' => __( 'Footer Menu' ),
            'primary' => __( 'Menu Principal' ),
        )
    );
}

function admin_default_page() {
    return 'http://localhost:9990/wp-admin';
}
  
//add_filter('login_redirect', 'admin_default_page');
add_action( 'init', 'create_rubros_hierarchical_taxonomy', 0 );
add_action( 'init', 'create_posttype' );
//add_action( 'admin_menu', 'remove_menus' );
add_action( 'init', 'register_menus' );

add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

function special_nav_class ($classes, $item) {
  if (in_array('current-menu-item', $classes) ){
    $classes[] = 'active ';
  }
  return $classes;
}
