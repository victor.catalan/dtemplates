<?php get_header(); ?>
<?php include get_theme_file_path( 'template-parts/page-tile.php' );  ?>

<div id="servicios-<?php the_ID(); ?>" class="main-content-area servicios">
<?php while ( have_posts() ) :
	the_post();
	get_template_part( 'template-parts/content-page-servicios' );  ?>
	<div class="separator separator-rouned2">
		<i class="fa fa-cog fa-spin2"></i>
	</div>
<?php endwhile; ?>
</div>
<?php get_footer(); ?>
