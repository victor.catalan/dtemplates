<?php get_header(); ?>
<?php include get_theme_file_path( 'template-parts/page-tile.php' );  ?>

<div id="productos-<?php the_ID(); ?>" class="main-content-area productos">
<section class="">
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-md-9">
              <?php include get_theme_file_path( 'template-parts/product-list.php' );  ?>
            </div>
            <div class="col-md-3">
             <div class="sidebar sidebar-right mt-sm-30">
                <div class="widget">
                  <h5 class="widget-title">Search box</h5>
                  <div class="search-form">
                    <form>
                      <div class="input-group">
                        <input type="text" placeholder="Click to Search" class="form-control search-input">
                        <span class="input-group-btn">
                        <button type="submit" class="btn search-button"><i class="fa fa-search"></i></button>
                        </span>
                      </div>
                    </form>
                  </div>
                </div>
                <div class="widget">
                  <h4 class="widget-title">Latest Products</h4>
                  <ul class="product_list_widget">
                    <li>
                      <div class="product-left">
                        <a href="shop-product-details.html">
                          <img width="100" height="100" src="<?php echo get_template_directory_uri(); ?>/assets/images/shop/product.jpg" class="thumbnail" alt="images">
                        </a>
                      </div>
                      <div class="product-right">
                        <a class="product-title" href="shop-product-details.html">Beanie with Logo</a>
                        <span class="amount"><span class="currencySymbol">£</span>11.05</span></div>
                    </li>
                    <li>
                      <div class="product-left">
                        <a href="shop-product-details.html">
                          <img width="100" height="100" src="<?php echo get_template_directory_uri(); ?>/assets/images/shop/product2.jpg" class="thumbnail" alt="images">
                        </a>
                      </div>
                      <div class="product-right">
                        <a class="product-title" href="shop-product-details.html">WordPress Pennant</a>
                        <del>
                          <span class="amount"><span class="currencySymbol">£</span>20.00</span>
                        </del>
                        <ins>
                          <span class="amount"><span class="currencySymbol">£</span>18.00</span>
                        </ins>
                    </div></li>
                    <li>
                      <div class="product-left">
                        <a href="shop-product-details.html">
                          <img width="100" height="100" src="<?php echo get_template_directory_uri(); ?>/assets/images/shop/product.jpg" class="thumbnail" alt="images">
                        </a>
                      </div>
                      <div class="product-right">
                        <a class="product-title" href="shop-product-details.html">Hoodie with Zipper</a>
                        <span class="amount"><span class="currencySymbol">£</span>11.05</span></div>
                    </li>
                  </ul>
                </div>
                <div class="widget widget_categories">
                  <h4 class="widget-title widget-title-line-bottom line-bottom-theme-colored1">Categories</h4>
                  <ul>
                    <li class="cat-item"><a href="#">Anxiety</a> </li>
                    <li class="cat-item"><a href="#">Grief and loss</a> </li>
                    <li class="cat-item"><a href="#">Uncategorized</a> </li>
                  </ul>
                </div>
                <div class="widget widget_tag_cloud">
                  <h4 class="widget-title widget-title-line-bottom line-bottom-theme-colored1">Tags</h4>
                  <div class="tagcloud">
                    <a href="#" class="tag-cloud-link">health</a>
                    <a href="#" class="tag-cloud-link">medical</a>
                    <a href="#" class="tag-cloud-link">news</a>
                    <a href="#" class="tag-cloud-link">latest</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
</div>
<?php get_footer(); ?>
