<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
	<meta name="description" content="<?php echo get_bloginfo('name'); ?>"/>
	<meta name="keywords" content="<?php echo get_bloginfo('name'); ?>"/>
	<meta name="author" content="device.cl"/>
  <title><?php echo get_bloginfo('name'); ?></title>
  <link rel="profile" href="http://gmpg.org/xfn/11" />
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
  <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
	

	<!-- Favicon and Touch Icons -->
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/favicon-16x16.png">
  <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/site.webmanifest">
  <link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/assets/images/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/assets/images/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<!-- Stylesheet -->
	<link href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo get_template_directory_uri(); ?>/assets/css/animate.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo get_template_directory_uri(); ?>/assets/css/javascript-plugins-bundle.css" rel="stylesheet"/>
  <link href="<?php echo get_template_directory_uri(); ?>/assets/css/font-awesome/font-awesome-5.15.3.min.css" rel="stylesheet" type="text/css">
	<!-- CSS | menuzord megamenu skins -->
	<link href="<?php echo get_template_directory_uri(); ?>/assets/js/menuzord/css/menuzord.css" rel="stylesheet"/>

	<!-- CSS | Main style file -->
	<link href="<?php echo get_template_directory_uri(); ?>/assets/css/style-main.css" rel="stylesheet" type="text/css">
	<link id="menuzord-menu-skins" href="<?php echo get_template_directory_uri(); ?>/assets/css/menuzord-skins/menuzord-rounded-boxed.css" rel="stylesheet"/>
  
	<!-- CSS | Responsive media queries -->
	<link href="<?php echo get_template_directory_uri(); ?>/assets/css/responsive.css" rel="stylesheet" type="text/css">
	<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->

	<!-- CSS | Dark Layout -->
	<link href="<?php echo get_template_directory_uri(); ?>/assets/css/style-main-dark.css" rel="stylesheet" type="text/css">
  
	<!-- CSS | Theme Color -->
	<link href="<?php echo get_template_directory_uri(); ?>/assets/css/colors/theme-skin-color-set1.css" rel="stylesheet" type="text/css">

	<link href="<?php echo get_template_directory_uri(); ?>/assets/css/colors/estilos.css?3" rel="stylesheet" type="text/css">


<!-- REVOLUTION STYLE SHEETS -->
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/js/revolution-slider/css/rs6.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/js/revolution-slider/extra-rev-slider1.css">
<!-- REVOLUTION LAYERS STYLES -->




	<link href="<?php echo get_template_directory_uri(); ?>/style.css?<?php echo rand(100, 999999); ?>" rel="stylesheet" type="text/css">

  <style>
    
    section, .header-nav-container,
    .footer.footer-currenty-style, .footer.footer-currenty-style .footer-bottom,
    #top-primary-nav-clone, #top-primary-nav-clone.menuzord-responsive .menuzord-menu > li > a{
      background-image:url('<?php echo get_template_directory_uri(); ?>/assets/images/bg/bg-08.png');
      background-attachment: inherit;
    }
    
    section.bg-theme-colored1{
      background-image:url('<?php echo get_template_directory_uri(); ?>/assets/images/bg/textura1.png');
    }
    section.page-title{
      background-color: rgba(0 , 0, 0, 1);
      background-image:url('<?php echo get_template_directory_uri(); ?>/assets/images/bg/bg-01.png');
      background-attachment: fixed;
      background-repeat: repeat;
    }
    
  </style>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
  <?php wp_head(); ?>
</head>

<body <?php //body_class(); ?>  class="tm-container-1300px has-side-panel side-panel-right dark-layout">
<?php wp_body_open(); ?>
<?php
  $page_home = get_page_by_title('Home');//get_page_by_path('/home', OBJECT, 'page');
  $url_instagram = CFS()->get( 'url_instagram', $page_home->ID, array( 'format' => 'api' ));
  $direccion_principal = CFS()->get( 'direccion_principal', $page_home->ID, array( 'format' => 'api' ));
  $correo_principal = CFS()->get( 'correo_principal', $page_home->ID, array( 'format' => 'api' ));
  $correo_secundario = CFS()->get( 'correo_secundario', $page_home->ID, array( 'format' => 'api' ));
  $telefono_principal = CFS()->get( 'telefono_principal', $page_home->ID, array( 'format' => 'api' ));
?>
<!-- Header -->
<header id="header" class="header header-layout-type-header-2rows">
    <?php 
    if( 
      (!empty($telefono_principal) && $telefono_principal != '') ||
      (!empty($correo_principal) && $correo_principal != '') ||
      (!empty($direccion_principal) && $direccion_principal != '')
    ){  
    ?>
    <div class="header-top">
      <div class="container">
        <div class="row">
          <div class="col-xl-auto header-top-left align-self-center text-center text-xl-start">
            <ul class="element contact-info">
              <?php if(!empty($telefono_principal) && $telefono_principal != ''){  ?>
                <li class="contact-phone text-dark"><i class="fa fa-phone font-icon sm-display-block"></i> Tel: <?php echo $telefono_principal; ?></li>
              <?php } ?>  
              <?php if(!empty($correo_principal) && $correo_principal != ''){  ?>
              <li class="contact-email text-dark"><i class="fa fa-envelope font-icon sm-display-block"></i> <?php echo $correo_principal; ?></li>
              <?php } ?>
              <?php if(!empty($correo_secundario) && $correo_secundario != ''){  ?>
              <li class="contact-email text-dark"><i class="fa fa-envelope font-icon sm-display-block"></i> <?php echo $correo_secundario; ?></li>
              <?php } ?>
              <?php if(!empty($direccion_principal) && $direccion_principal != ''){  ?>
                <li class="contact-address text-dark"><i class="fa fa-map font-icon sm-display-block"></i> <?php echo $direccion_principal; ?></li>
              <?php } ?>               
            </ul>
          </div>
          <div class="col-xl-auto ms-xl-auto header-top-right align-self-center text-center text-xl-end d-none d-sm-block">
            <div class="element pt-0 pb-0">
              <ul class="styled-icons icon-dark icon-theme-colored icon-circled clearfix">
                <li><a class="social-link" target="_blank" href="<?php echo $url_instagram; ?>" ><i class="fab fa-instagram text-white"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php } ?>
    <div class="header-nav tm-enable-navbar-hide-on-scroll">
      <div class="header-nav-wrapper navbar-scrolltofixed">
        <div class="menuzord-container header-nav-container">
          <div class="container position-relative">
            <div class="row header-nav-col-row">
              <div class="col-sm-auto align-self-center">
                <a class="menuzord-brand site-brand" href="/">
                  <img class="logo-default logo-1x" src="<?php echo get_template_directory_uri(); ?>/assets/images/logos/logo_primary_gold_400x200.png" alt="Logo">
                  <img class="logo-default logo-2x retina" src="<?php echo get_template_directory_uri(); ?>/assets/images/logos/logo_primary_gold_400x200.png" alt="Logo">
                  <span class="logo-txt d-none d-sm-block">
                    <?php //echo get_bloginfo('name'); ?>
                  </span>
                  <span class="logo-txt d-block d-sm-none">
                    <?php //echo get_bloginfo('name'); ?>
                  </span>
                </a>
              </div>
              <div class="col-sm-auto ms-auto pr-0 align-self-center d-block d-sm-none">
                <a class="social-link instagram" target="_blank" href="<?php echo $url_instagram; ?>" ><i class="fab fa-instagram text-white"></i></a>
              </div>
              <div class="col-sm-auto ms-auto pr-0 align-self-center">
                <?php
                  wp_nav_menu(
                    array(
                      'menu' => 'primary',
                      'theme_location' => 'primary',
                      'container_class'=>'menuzord theme-color2',
                      'container_id' => 'top-primary-nav',
                      'menu_class' => 'menuzord-menu',
                    )
                  );
                ?>
              </div>
            </div>
            <div class="row d-block d-xl-none">
               <div class="col-12">
                <nav id="top-primary-nav-clone" class="menuzord d-block d-xl-none default menuzord-color-default menuzord-border-boxed menuzord-responsive" data-effect="slide" data-animation="none" data-align="right">
                 <ul id="main-nav-clone" class="menuzord-menu menuzord-right menuzord-indented scrollable">
                 </ul>
                </nav>
               </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  <div id="wrapper" class="clearfix">