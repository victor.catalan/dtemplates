<?php
  $page_home = get_page_by_title('Home');//get_page_by_path('/home', OBJECT, 'page');
  $texto_footer = CFS()->get( 'texto_footer', $page_home->ID, array( 'format' => 'api' ));

?>
  <div>
  <?php if($texto_footer){ ?>
  <section class="bg-theme-colored1 section-typo-light">
      <div class="container">
        <div class="row">
          <div class="col-md-1">
            <i class="fa fa-comments font-size-64"></i>
          </div>
          <div class="col-md-8">
            <p class="lead">
              <?php echo $texto_footer; ?>
            </p>
          </div>
          <div class="col-md-3">
            <div class="text-center"> 
              <a class="btn btn-outline-light btn-lg btn-round" href="/contacto" target="_self">
                Contáctanos
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <?php } ?>
  
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
</div>
<!-- Footer -->
<footer id="footer" class="footer footer-currenty-style bg-size-cover bg-no-repeat">
    <div class="footer-widget-area">
      <div class="footer-top-part">
        
      </div>
      <div class="footer-bottom">
        <div class="container">
          <div class="row pt-40 pb-40 justify-content-center">
            <div class="col-sm-6">
              <div class="footer-paragraph text-center">
                <b class="text-warn">
                © <?php echo date('Y'); ?>
                </b> 
                <span class="text-primary">
                  <?php echo get_bloginfo('name'); ?>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>

<!-- Footer Scripts -->

<!-- external javascripts -->
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/popper.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/javascript-plugins-bundle.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/menuzord/js/menuzord.js"></script>

<!-- REVOLUTION JS FILES -->
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/revolution-slider/js/revolution.tools.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/revolution-slider/js/rs6.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/revolution-slider/extra-rev-slider1.js"></script>

<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.validate.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/messages_es.min.js"></script>

<!-- JS | Custom script for all pages -->
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/custom.js?<?php echo rand(100, 999999); ?>"></script>
<!-- Contact Form Validation-->
<script>
    (function($) {
      $("#contact_form").validate({
        submitHandler: function(form) {
          var form_btn = $(form).find('button[type="submit"]');
          var form_result_div = '#form-result';
          $(form_result_div).remove();
          form_btn.before('<div id="form-result" class="alert alert-success" role="alert" style="display: none;"></div>');
          var form_btn_old_msg = form_btn.html();
          form_btn.html(form_btn.prop('disabled', true).data("loading-text"));
          $(form).ajaxSubmit({
            type: 'POST',
            //dataType:  'json',
            success: function(data) {
              console.log('data',data);
              var msg = '';
              if( data == 'true' ) {
                $(form).find('.form-control').val('');
                msg = "Enviado con éxito";
              }else{
                msg = "Hubo errores al enviar";
              }
              form_btn.prop('disabled', false).html(form_btn_old_msg);
              $(form_result_div).html(msg).fadeIn('slow');
              setTimeout(function(){ $(form_result_div).fadeOut('slow') }, 6000);
            }
          });
        }
      });
    })(jQuery);
  </script>
<?php wp_footer(); ?>

</body>
</html>
