<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

get_header();
?>

<section id="404_mensaje" class="bg-black-222 pb-5 pt-0" style="">
	<div class="container pt-3">
		<div class="row">
			<div class="col-md-12">
				<div class="error-template text-center">
					<h1>Error 404!</h1>
					<h2>El enlace que buscas no existe</h2>
					<div class="error-details mb-3">
						Por favor intenta nuevamente o ingresa a nuestra página de inicio
					</div>
					<div class="error-actions row row-cols-2">
						<a href="<?php echo get_home_url(); ?>" class="btn btn-gray btn-outline btn-lg btn-round">
							<span class="fa fa-home"></span>
							Ir al inicio
						</a>
						<a href="<?php echo get_permalink( get_page_by_path( 'contacto' ) ); ?>" class="btn btn-outline-theme-colored1 btn-outline btn-lg btn-round">
							<span class="fa fa-envelope"></span> Contáctanos
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>

</section>

<?php
get_footer();

