<?php 

//Set auto:
// activar theme
// activar plugins
// conf permalinks
// set tagline
// create home set as homepage


$current_user = wp_get_current_user();
$current_role = $current_user->roles[0];

function formato_numero($numero, $decimales=0){
    return number_format($numero, $decimales, ',', '.');
}



function create_rubros_hierarchical_taxonomy() { 
    $labels = array(
        'name' => _x( 'Categorias Producto', 'Categorias del producto' ),
        'singular_name' => _x( 'Categoria Producto', 'taxonomy singular name' ),
        'search_items' =>  __( 'Buscar Categorias Producto' ),
        'all_items' => __( 'Todas las Categorias Producto' ),
        'parent_item' => __( 'Padre Categoria Producto' ),
        'parent_item_colon' => __( 'Padre Categoria Producto:' ),
        'edit_item' => __( 'Editar Categoria Producto' ), 
        'update_item' => __( 'Actualizar Categoria Producto' ),
        'add_new_item' => __( 'Agregar nueva Categoria Producto' ),
        'new_item_name' => __( 'Nombre nueva Categoria Producto' ),
        'menu_name' => __( 'Categorias Producto' ),
    );    
    register_taxonomy('categorias-producto',array('productos'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_in_rest' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'categorias-producto' ),
    ));

    $labels = array(
        'name' => _x( 'Materiales', 'Materiales del producto' ),
        'singular_name' => _x( 'Material', 'taxonomy singular name' ),
        'search_items' =>  __( 'Buscar Materiales' ),
        'all_items' => __( 'Todas las Materiales' ),
        'parent_item' => __( 'Padre Material' ),
        'parent_item_colon' => __( 'Padre Material:' ),
        'edit_item' => __( 'Editar Material' ), 
        'update_item' => __( 'Actualizar Material' ),
        'add_new_item' => __( 'Agregar nuevo Material' ),
        'new_item_name' => __( 'Nombre nuevo Material' ),
        'menu_name' => __( 'Materiales' ),
    );    
    register_taxonomy('materiales',array('productos'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_in_rest' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'materiales' ),
    ));

  
}

function create_posttype() {
    
    register_post_type( 'productos',
        array(
            'labels' => array(
                'name' => __( 'Productos' ),
                'singular_name' => __( 'Producto' ),
                'search_items' =>  __( 'Buscar Productos' ),
                'all_items' => __( 'Todos los Productos' ),
                'parent_item' => __( 'Padre Producto' ),
                'parent_item_colon' => __( 'Padre Producto:' ),
                'edit_item' => __( 'Editar Producto' ), 
                'update_item' => __( 'Actualizar Producto' ),
                'add_new_item' => __( 'Agregar nuevo Producto' ),
                'new_item_name' => __( 'Nombre nuevo Producto' ),
                'menu_name' => __( 'Productos' ),
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'productos'),
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => true,
            'show_in_rest' => true,

        )
    );
}


function remove_menus(){  

    //remove_menu_page( 'index.php' );                  //Dashboard  
    if($current_role != 'editor'){
        
    }    
    remove_menu_page( 'edit.php' );                   //Posts  
    remove_menu_page( 'upload.php' );                 //Media  
    remove_menu_page( 'edit.php?post_type=page' );    //Pages  
    remove_menu_page( 'edit.php?post_type=productos' );    //Pages  
    remove_menu_page( 'edit-comments.php' );          //Comments  

    remove_menu_page( 'wpcf7' );    //Pages  
    remove_menu_page( 'themes.php' );                 //Appearance  
    remove_menu_page( 'plugins.php' );                //Plugins  
    remove_menu_page( 'users.php' );                  //Users  
    remove_menu_page( 'tools.php' );                  //Tools  
    remove_menu_page( 'options-general.php' );        //Settings 
  
}  

function register_menus() {
    register_nav_menus(
        array(
            'header-menu' => __( 'Header Menu' ),
            'footer-menu' => __( 'Footer Menu' ),
            'primary' => __( 'Menu Principal' ),
        )
    );
}

function admin_default_page() {
    return 'http://localhost:9701/wp-admin';
}

function wpb_remove_version() {
    return '';
}

function special_nav_class ($classes, $item) {
    if (in_array('current-menu-item', $classes) ){
        $classes[] = 'active ';
    }
    return $classes;
}

function no_wordpress_errors(){
    return 'Algo salió mal!';
}

//add_filter('login_redirect', 'admin_default_page');
add_action( 'init', 'create_rubros_hierarchical_taxonomy', 0 );
add_action( 'init', 'create_posttype' );
add_action( 'init', 'register_menus' );
//remove_action('welcome_panel', 'wp_welcome_panel');
add_filter('the_generator', 'wpb_remove_version');
add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
add_filter( 'login_errors', 'no_wordpress_errors' );


if(!is_admin()){
    add_action( 'admin_menu', 'remove_menus' );
}


// function wpb_admin_account(){
// $user = 'Username';
// $pass = 'Password';
// $email = 'email@domain.com';
// if ( !username_exists( $user )  && !email_exists( $email ) ) {
// $user_id = wp_create_user( $user, $pass, $email );
// $user = new WP_User( $user_id );
// $user->set_role( 'administrator' );
// } }
// add_action('init','wpb_admin_account');