<!-- Isotope Filter -->
<div class="isotope-layout-filter filter-style-8 cat-filter-default text-start" data-link-with="product-gallery-holder-1111">
    <a href="#" class="active" data-filter="*">Todos</a>
    <?php
    $categorias_producto = get_terms( array(
        'taxonomy' => 'categorias-producto',
        'hide_empty' => false,
    ) );
    $i=1;
    ?>
    <?php foreach ( $categorias_producto as $categoria_producto ) : ?>
        <a href="#" class="" data-filter=".<?php echo($categoria_producto->slug); ?>"><?php echo($categoria_producto->name); ?></a>
    <?php $i++; ?>
    <?php endforeach; ?>
    </div>
    <!-- End Isotope Filter -->

    <!-- Isotope Gallery Grid -->
    <div id="product-gallery-holder-1111" class="isotope-layout grid-4 gutter-15 clearfix lightgallery-lightbox">
    <div class="isotope-layout-inner" style="position: relative; height: 864.876px;">
    <?php
    $loop = new WP_Query( array(
        'post_type' => 'productos',
        'posts_per_page' => -1
        )
    );
    ?>
    <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
        <?php 
        $taxonomy = 'categorias-producto';
        //$clientes = CFS()->get( 'imagenes_elemento', get_the_ID(), array( 'format' => 'api' ));
        /*$terms = get_object_term_cache( $post->ID, $taxonomy );
        if ( false === $terms ) {
            $terms = wp_get_object_terms( $post->ID, $taxonomy );
            if ( ! is_wp_error( $terms ) ) {
                $term_ids = wp_list_pluck( $terms, 'term_id' );
                wp_cache_add( $post->ID, $term_ids, $taxonomy . '_relationships' );
            }
        }*/
        $id = get_the_ID();
        $terms = wp_get_object_terms( $post->ID, $taxonomy );
        $cats_classes = '';
        $cats_names = '';
        foreach($terms as $t){
            $cats_classes .= ' '.$t->slug;
            $cats_names .= ' '.$t->name;
        }
        $descripcion = CFS()->get( 'descripcion', get_the_ID(), array( 'format' => 'api' ));
        $precio = CFS()->get( 'precio', get_the_ID(), array( 'format' => 'api' ));
        $descuento = CFS()->get( 'descuento', get_the_ID(), array( 'format' => 'api' ));
        $precio_descuento = $precio - ($precio * 0.01 * $descuento);
        $slug = get_post_field( 'post_name', get_post() );
        $imagenes = CFS()->get( 'imagenes_elemento', get_the_ID(), array( 'format' => 'api' ));
        ?>
        <!-- Isotope Item Start -->
        <div class="isotope-item <?php echo $cats_classes; ?>" style="position: absolute; left: 0px; top: 0px;">
            <div class="isotope-item-inner">
            <div class="product">
                <div class="product-header">
                <div class="thumb image-swap">
                    <a href="shop-product-details.html"><img src="<?php echo $imagenes[0]['imagen_elemento']; ?>" class="product-main-image img-responsive img-fullwidth" width="300" height="300" alt="product"></a>
                    <a href="shop-product-details.html"><img src="<?php echo $imagenes[0]['imagen_elemento']; ?>" class="product-hover-image img-responsive img-fullwidth" alt="product"></a>
                </div>
                <div class="product-button-holder">
                    <ul class="shop-icons">
                    <li class="item"><a href="#" class="button btn-quickview" title="Ver producto"><i class="fa fa-eye"></i></a></li>
                    <li class="item"><a href="shop-cart.html" class="button tm-btn-add-to-cart" title="Agregar al carro"><i class="fa fa-cart-plus"></i></a></li>
                    </ul>
                </div>
                </div>
                <div class="product-details">
                <span class="product-categories"><a href="#" rel="tag"><?php echo $cats_names; ?></a></span>
                <h5 class="product-title">
                    <a href="productos/<?php echo $slug; ?>">
                        <?php the_title();?>
                    </a>
                </h5>
                <span class="price">
                    <?php if ( $descuento > 0 ) { ?>
                    <del><span class="amount"><span class="currency-symbol">$</span><?php echo formato_numero($precio); ?></span></del>
                    <?php } ?>
                    <ins><span class="amount"><span class="currency-symbol">$</span><?php echo formato_numero($precio_descuento); ?></span></ins>
                </span>
                </div>
            </div>
            </div>
        </div>
        <!-- Isotope Item End -->
    <?php endwhile; wp_reset_query(); ?>  
    

    </div>
</div>
    <!-- End Isotope Gallery Grid -->