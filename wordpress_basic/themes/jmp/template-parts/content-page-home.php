<div id="post-<?php the_ID(); ?>" class="main-content-area">
<?php
  $mapa_url = CFS()->get( 'url_mapa', get_the_ID(), array( 'format' => 'api' ));
?>

	<!-- Section: home Start -->
    <section id="home" class="bg-black">
      <div class="container-fluid p-0">
        <div class="row">
          <div class="col">
            <!-- START Droled HTML RevSlider REVOLUTION SLIDER 6.2.19 -->
            <p class="rs-p-wp-fix"></p>
            <rs-module-wrap id="rev_slider_1_1_wrapper" data-alias="droled-html-revslider" data-source="gallery" style="background:transparent;padding:0;margin:0px auto;margin-top:0;margin-bottom:0;">
              <rs-module id="rev_slider_1_1" style="" data-version="6.2.19">
              <rs-slides>
                  <?php
                    $imagenes_galeria = CFS()->get( 'imagenes_galeria', get_the_ID(), array( 'format' => 'api' ));
                    foreach ($imagenes_galeria as $imagen_galeria) { ?>
                    <!-- <div class="item">
                      <img class="client-thumb" src="<?php echo $imagen_galeria['imagen_galeria']; ?>" alt="<?php echo the_title(); ?>">
                    </div>-->
                  
                    <rs-slide data-key="rs-<?php echo $i; ?>" data-title="Fundición Las Rosas" data-thumb="<?php echo $imagen_galeria['imagen_galeria']; ?>" data-anim="ei:d;eo:d;s:d;r:0;t:slotslide-horizontal;sl:d;">
                      <img src="<?php echo $imagen_galeria['imagen_galeria']; ?>" title="b" width="1920" height="800" data-parallax="off" class="rev-slidebg" data-no-retina>
                      <rs-layer
                        id="slider-14-slide-<?php echo $i; ?>-layer-10"
                        data-type="text"
                        data-rsp_ch="on"
                        data-xy="x:c;yo:194px,178px,174px,171px;"
                        data-text="w:normal;s:24,22,20,18;l:30,28,24,25;fw:500;a:center;"
                        data-dim="w:427px,406px,352px,354px;"
                        data-frame_1="st:500;sp:1000;sR:500;"
                        data-frame_999="o:0;st:w;sR:7500;"
                        class="font-current-theme1"
                        style="z-index:11;ext-transform:capitalize;"
                        ><?php echo $imagen_galeria['subtexto_image']; ?>
                      </rs-layer>
                      <rs-layer
                        id="slider-14-slide-<?php echo $i; ?>-layer-18"
                        data-type="text"
                        data-rsp_ch="on"
                        data-xy="x:c;yo:258px,239px,225px,223px;"
                        data-text="w:normal;s:80,72,64,42;l:105,85,86,64;ls:0px,0px,-1px,0px;fw:800;a:center;"
                        data-dim="w:auto,auto,auto,455px;"
                        data-frame_1="st:1100;sp:1000;sR:1100;"
                        data-frame_999="o:0;st:w;sR:6900;"
                        class="font-current-theme1"
                        style="z-index:10;"
                        ><?php echo $imagen_galeria['texto_imagen']; ?>
                      </rs-layer>
                    </rs-slide>
                  <?php 
                      $i++;
                  }
                  wp_reset_query(); ?>
                </rs-slides>
                <rs-static-layers>
                  <!--
                    -->
                </rs-static-layers>
              </rs-module>
              <script type="text/javascript">
                if(typeof revslider_showDoubleJqueryError === "undefined") {
                  function revslider_showDoubleJqueryError(sliderID) {
                    var err = "<div class='rs_error_message_box'>";
                    err += "<div class='rs_error_message_oops'>Oops...</div>";
                    err += "<div class='rs_error_message_content'>";
                    err += "You have some jquery.js library include that comes after the Slider Revolution files js inclusion.<br>";
                    err += "To fix this, you can:<br>&nbsp;&nbsp;&nbsp; 1. Set 'Module General Options' -> 'Advanced' -> 'jQuery & OutPut Filters' -> 'Put JS to Body' to on";
                    err += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jQuery.js inclusion and remove it";
                    err += "</div>";
                  err += "</div>";
                    var slider = document.getElementById(sliderID); slider.innerHTML = err; slider.style.display = "block";
                  }
                }
              </script>
            </rs-module-wrap>
            <!-- END REVOLUTION SLIDER -->
          </div>
        </div>
      </div>
    </section>
    <section class="d-none">
      <div class="container pt-30 pb-30">
        <div class="row">
          <div class="col-md-3">
            <div class="tm-sc-icon-box icon-box icon-left iconbox-centered-in-responsive mb-lg-30">
              <div class="icon-box-wrapper">
                <a class="icon">
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/iconos/anillo02_primary.png" alt="Image">
                </a>
                <div class="icon-text">
                  <h5 class="icon-box-title mt-0">Free Shipping</h5>
                  <div class="content"><p>All Order Free</p></div>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="tm-sc-icon-box icon-box icon-left iconbox-centered-in-responsive mb-lg-30">
              <div class="icon-box-wrapper">
                <a class="icon">
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/iconos/anillo01_primary.png" alt="Image">
                </a>
                <div class="icon-text">
                  <h5 class="icon-box-title mt-0">Support 24/7</h5>
                  <div class="content"><p>Call +01 2345 6789</p></div>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="tm-sc-icon-box icon-box icon-left iconbox-centered-in-responsive mb-lg-30">
              <div class="icon-box-wrapper">
                <a class="icon">
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/iconos/piedra02_primary.png" alt="Image">
                </a>
                <div class="icon-text">
                  <h5 class="icon-box-title mt-0">15 Days Returns</h5>
                  <div class="content"><p>Buyer Protection</p></div>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="tm-sc-icon-box icon-box icon-left iconbox-centered-in-responsive mb-lg-30">
              <div class="icon-box-wrapper">
                <a class="icon">
                  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/iconos/collar03_primary.png" alt="Image">
                </a>
                <div class="icon-text">
                  <h5 class="icon-box-title mt-0">Secure Checkout</h5>
                  <div class="content"><p>Fastest Payment</p></div>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>


    <section>
      <div class="container">
        <div class="section-title text-center mb-30">
          <div class="row">
            <div class="col-md-12">
              <h2>Nuevos <span class="text-warn">Productos</span></h2>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <?php include get_theme_file_path( 'template-parts/product-list.php' );  ?>
          </div>
        </div>
      </div>
    </section>


	<!-- Section: Features -->
    <section class="features-section bg-black-333 d-none">
      <div class="container-fluid pt-30 pb-30">
        <div class="section-content">
          <div class="row">
            <div class="col-lg-6 col-xl-4">
              <div class="feature-current-item mb-lg-30 bg-dark">
                <div class="inner-box">
                  <h6 class="feature-sub-title text-uppercase">Nuestros Productos</h6>
                  <h4 class="feature-title">We Always Building Quality Industrial</h4>
                </div>
                <div class="feature-icon">
                <img class="logo-default retina" style="width:75%;" src="<?php echo get_template_directory_uri(); ?>/assets/images/iconos/piedra01_primary.png" alt="Logo">
                </div>
              </div>
            </div>
            <div class="col-lg-6 col-xl-4">
              <div class="feature-current-item mb-lg-30 bg-dark">
                <div class="inner-box">
                  <h6 class="feature-sub-title text-uppercase">Nuestros Productos</h6>
                  <h4 class="feature-title">Best Manufacturing Service Provider</h4>
                </div>
                <div class="feature-icon">
                <img class="logo-default retina" style="width:75%;" src="<?php echo get_template_directory_uri(); ?>/assets/images/iconos/collar03_primary.png" alt="Logo">
                </div>
              </div>
            </div>
            <div class="col-lg-6 col-xl-4">
              <div class="feature-current-item bg-dark">
                <div class="inner-box">
                  <h6 class="feature-sub-title text-uppercase">Contáctanos</h6>
                  <h4 class="feature-title">Experience Trusted Best Contractors</h4>
                </div>
                <div class="feature-icon">
                <img class="logo-default retina" style="width:75%;" src="<?php echo get_template_directory_uri(); ?>/assets/images/iconos/collar03_primary.png" alt="Logo">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    
</div>

