<div id="post-<?php the_ID(); ?>" class="main-content-area">
	<section class="d-none page-title tm-page-title page-title-standard parallaxr layer-overlay bg-img-center" data-tm-bg-img="images/bg/metalurgia.jpg" style="background-image: url(&quot;images/bg/textura1.png&quot;);">
		<div class="container pt-80 pb-80">
			<div class="row">
				<div class="col-md-8 title-content sm-text-center">
					<h3 class="title"><?php the_title(); ?></h3>
				</div>
				<div class="col-md-4 title-content text-center">
					<nav class="breadcrumbs" role="navigation" aria-label="Breadcrumbs">
						<div class="breadcrumbs">
							<span><a href="/" rel="home">Home Producto</a></span>
							<span><i class="fa fa-angle-right"></i></span>
							<span class="active"><?php the_title(); ?></span>
						</div>
					</nav>
				</div>
			</div>
		</div>
	</section>
	<section>
      <div class="container">
        <div class="section-content">
          <div class="product-single">
            <div class="row">
              <div class="col-md-6">
                <div class="product-image-slider lightgallery-lightbox">
                  <div class="tm-owl-thumb-carousel owl-carousel owl-theme owl-loaded owl-drag" data-nav="true" data-slider-id="1">
                    
                    
                    
                  <div class="owl-stage-outer owl-height" style="height: 557px;"><div class="owl-stage" style="transform: translate3d(-1671px, 0px, 0px); transition: all 0.25s ease 0s; width: 3899px;"><div class="owl-item cloned" style="width: 557px;"><div data-thumb="<?php echo get_template_directory_uri(); ?>/assets/images/productos/prod01_01.jpeg">
                      <a class="lightgallery-trigger" data-exthumbimage="<?php echo get_template_directory_uri(); ?>/assets/images/productos/prod01_01.jpeg" data-src="<?php echo get_template_directory_uri(); ?>/assets/images/productos/prod01_01.jpeg" title="Product 2" href="<?php echo get_template_directory_uri(); ?>/assets/images/productos/prod01_01.jpeg"><img class="img-fullwidth" src="<?php echo get_template_directory_uri(); ?>/assets/images/productos/prod01_01.jpeg" alt="images"></a>
                    </div></div><div class="owl-item cloned" style="width: 557px;"><div data-thumb="<?php echo get_template_directory_uri(); ?>/assets/images/productos/prod02_01.jpeg">
                      <a class="lightgallery-trigger" data-exthumbimage="<?php echo get_template_directory_uri(); ?>/assets/images/productos/prod02_01.jpeg" data-src="<?php echo get_template_directory_uri(); ?>/assets/images/productos/prod02_01.jpeg" title="Product 3" href="<?php echo get_template_directory_uri(); ?>/assets/images/productos/prod02_01.jpeg"><img class="img-fullwidth" src="<?php echo get_template_directory_uri(); ?>/assets/images/productos/prod02_01.jpeg" alt="images"></a>
                    </div></div><div class="owl-item" style="width: 557px;"><div data-thumb="<?php echo get_template_directory_uri(); ?>/assets/images/productos/prod02_01.jpeg">
                      <a class="lightgallery-trigger" data-exthumbimage="<?php echo get_template_directory_uri(); ?>/assets/images/productos/prod02_01.jpeg" data-src="<?php echo get_template_directory_uri(); ?>/assets/images/productos/prod02_01.jpeg" title="Product 1" href="<?php echo get_template_directory_uri(); ?>/assets/images/productos/prod02_01.jpeg"><img class="img-fullwidth" src="<?php echo get_template_directory_uri(); ?>/assets/images/productos/prod02_01.jpeg" alt="images"></a>
                    </div></div><div class="owl-item active" style="width: 557px;"><div data-thumb="<?php echo get_template_directory_uri(); ?>/assets/images/productos/prod01_01.jpeg">
                      <a class="lightgallery-trigger" data-exthumbimage="<?php echo get_template_directory_uri(); ?>/assets/images/productos/prod01_01.jpeg" data-src="<?php echo get_template_directory_uri(); ?>/assets/images/productos/prod01_01.jpeg" title="Product 2" href="<?php echo get_template_directory_uri(); ?>/assets/images/productos/prod01_01.jpeg"><img class="img-fullwidth" src="<?php echo get_template_directory_uri(); ?>/assets/images/productos/prod01_01.jpeg" alt="images"></a>
                    </div></div><div class="owl-item" style="width: 557px;"><div data-thumb="<?php echo get_template_directory_uri(); ?>/assets/images/productos/prod02_01.jpeg">
                      <a class="lightgallery-trigger" data-exthumbimage="<?php echo get_template_directory_uri(); ?>/assets/images/productos/prod02_01.jpeg" data-src="<?php echo get_template_directory_uri(); ?>/assets/images/productos/prod02_01.jpeg" title="Product 3" href="<?php echo get_template_directory_uri(); ?>/assets/images/productos/prod02_01.jpeg"><img class="img-fullwidth" src="<?php echo get_template_directory_uri(); ?>/assets/images/productos/prod02_01.jpeg" alt="images"></a>
                    </div></div><div class="owl-item cloned" style="width: 557px;"><div data-thumb="<?php echo get_template_directory_uri(); ?>/assets/images/productos/prod02_01.jpeg">
                      <a class="lightgallery-trigger" data-exthumbimage="<?php echo get_template_directory_uri(); ?>/assets/images/productos/prod02_01.jpeg" data-src="<?php echo get_template_directory_uri(); ?>/assets/images/productos/prod02_01.jpeg" title="Product 1" href="<?php echo get_template_directory_uri(); ?>/assets/images/productos/prod02_01.jpeg"><img class="img-fullwidth" src="<?php echo get_template_directory_uri(); ?>/assets/images/productos/prod02_01.jpeg" alt="images"></a>
                    </div></div><div class="owl-item cloned" style="width: 557px;"><div data-thumb="<?php echo get_template_directory_uri(); ?>/assets/images/productos/prod01_01.jpeg">
                      <a class="lightgallery-trigger" data-exthumbimage="<?php echo get_template_directory_uri(); ?>/assets/images/productos/prod01_01.jpeg" data-src="<?php echo get_template_directory_uri(); ?>/assets/images/productos/prod01_01.jpeg" title="Product 2" href="<?php echo get_template_directory_uri(); ?>/assets/images/productos/prod01_01.jpeg"><img class="img-fullwidth" src="<?php echo get_template_directory_uri(); ?>/assets/images/productos/prod01_01.jpeg" alt="images"></a>
                    </div></div></div></div><div class="owl-nav"><button class="owl-prev"><i class="fa fa-chevron-left"></i></button><button class="owl-next"><i class="fa fa-chevron-right"></i></button></div><div class="owl-dots disabled"></div></div>
                  <ul class="tm-owl-thumbs" data-slider-id="1">
                    <li class="tm-owl-thumb-item"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/productos/prod02_01.jpeg" alt="images"></li>
                    <li class="tm-owl-thumb-item active"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/productos/prod01_01.jpeg" alt="images"></li>
                    <li class="tm-owl-thumb-item"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/productos/prod02_01.jpeg" alt="images"></li>
                  </ul>
                </div>
              </div>
              <div class="col-md-6">
                <div class="product-summary">
                  <h2 class="product-title mt-0"><?php echo get_the_title(); ?></h2>
                  <div class="product-rating">
                    <div class="star-rating" role="img" aria-label="Rated 5.00 out of 5"><span style="width:100%">Rated <strong class="rating">5.00</strong> out of 5 based on <span class="rating">1</span> customer rating</span></div>
                    <a href="#reviews" class="review-link" rel="nofollow">(<span class="count">1</span> customer review)</a>
                  </div>
                  <p class="price"><span class="amount"><span class="currency-symbol">$</span>42.00</span> – <span class="amount"><span class="currency-symbol">$</span>45.00</span></p>
                  <div class="short-description">
                    <p>It’s difficult to find examples of lorem ipsum in use before Letraset made it popular as a dummy text in the 1960s, although McClintock says he remembers coming across</p>
                    <p><strong>Size &amp; Fit</strong><br>
                    The model (height 6′) is perfect for you</p>
                    <p><strong>Material &amp; Care</strong><br>
                    100% Genuine<br>
                    Machine-wash</p>
                  </div>
                  <div class="product_meta">
                    <span class="sku_wrapper">SKU: <span class="sku" data-o_content="woo-hoodie">woo-hoodie</span></span>
                    <span class="posted_in">Category: <a href="http://pc1/kodesolution/wp/lovimselemenrtor/product-category/hoodies/" rel="tag">Hoodies</a></span>
                  </div>
                  <div class="btn-add-to-cart">
                    <div class="quantity">
                      <input class="minus" type="button" value="-">
                      <input type="number" id="quantity_5f0c6f4cb0b78" class="input-text qty text" step="1" min="1" max="" name="quantity" value="1" title="Qty" size="4" placeholder="" inputmode="numeric">
                      <input class="plus" type="button" value="+">
                    </div>
                    <a href="shop-cart.html" class="btn btn-theme-colored1 ml-10">Add to cart</a>
                  </div>
                </div>
              </div>
              <div class="col-md-12 mt-60">
                <div class="horizontal-tab product-tab">
                  <ul class="nav nav-tabs" id="nav-tab" role="tablist">
                    <li class="nav-item">
                      <button class="nav-link active" id="desc-tab" data-bs-toggle="tab" data-bs-target="#desc-content" role="tab" aria-controls="desc-content" aria-selected="true"><strong>Description</strong></button>
                    </li>
                    <li class="nav-item">
                      <button class="nav-link" id="addinfo-tab" data-bs-toggle="tab" data-bs-target="#addinfo-content" role="tab" aria-controls="addinfo-content" aria-selected="true"><strong>Additional Information</strong></button>
                    </li>
                    <li class="nav-item">
                      <button class="nav-link" id="reviews-tab" data-bs-toggle="tab" data-bs-target="#reviews-content" role="tab" aria-controls="reviews-content" aria-selected="true"><strong>Reviews</strong></button>
                    </li>
                  </ul>
                  <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active p-20" id="desc-content" role="tabpanel" aria-labelledby="desc-tab">
                      <p>One Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo ratione tempora consectetur quos minus voluptates nisi hic alias libero explicabo reiciendis sint ut quo nulla ipsa aliquid neque molestias et qui sunt. Odit, molestiae. One Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo ratione tempora consectetur quos minus voluptates nisi hic alias libero explicabo reiciendis sint ut quo nulla ipsa aliquid neque molestias et qui sunt. Odit, molestiae.</p>
                      <p>One Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo ratione tempora consectetur quos minus voluptates nisi hic alias libero explicabo reiciendis sint ut quo nulla ipsa aliquid neque molestias et qui sunt. Odit, molestiae.</p>
                    </div>
                    <div class="tab-pane fade p-20" id="addinfo-content" role="tabpanel" aria-labelledby="addinfo-tab">
                      <table class="table table-striped">
                        <tbody>
                          <tr>
                            <th>Brand</th>
                            <td><p>Envato</p></td>
                          </tr>
                          <tr>
                            <th>Color</th>
                            <td><p>Black</p></td>
                          </tr>
                          <tr>
                            <th>Size</th>
                            <td><p>Large, Medium</p></td>
                          </tr>
                          <tr>
                            <th>Weight</th>
                            <td>27 kg</td>
                          </tr>
                          <tr>
                            <th>Dimensions</th>
                            <td>16 x 22 x 123 cm</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="tab-pane fade p-20" id="reviews-content" role="tabpanel" aria-labelledby="reviews-tab">
                      <ol class="product-reviews">
                        <li class="review">
                          <div class="d-flex">
                            <div class="flex-shrink-0"><img class="thumb img-circle mr-3" alt="images" src="<?php echo get_template_directory_uri(); ?>/assets/images/shop/author.jpg" width="75"></div>
                            <div class="flex-grow-1 ps-3">
                              <ul class="review-meta list-inline">
                                <li>
                                  <div class="star-rating" role="img" aria-label="Rated 5 out of 5"><span style="width:100%">Rated <strong class="rating">5</strong> out of 5</span></div>
                                </li>
                                <li>
                                  <h5 class="review-heading"><span class="author">Tom Joe</span> <small>– Mar 15, 2021</small></h5>
                                </li>
                              </ul>
                              Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                            </div>
                          </div>
                        </li>
                        <li class="review">
                          <div class="d-flex">
                            <div class="flex-shrink-0"><img class="thumb img-circle mr-3" alt="images" src="<?php echo get_template_directory_uri(); ?>/assets/images/shop/author.jpg" width="75"></div>
                            <div class="flex-grow-1 ps-3">
                              <ul class="review-meta list-inline">
                                <li>
                                  <div class="star-rating" role="img" aria-label="Rated 5 out of 5"><span style="width:100%">Rated <strong class="rating">5</strong> out of 5</span></div>
                                </li>
                                <li>
                                  <h5 class="review-heading"><span class="author">Tom Joe</span> <small>– Mar 15, 2021</small></h5>
                                </li>
                              </ul>
                              Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                            </div>
                          </div>
                        </li>
                      </ol>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </section>
</div>

