<div id="post-<?php the_ID(); ?>" class="main-content-area">
<?php
  $page_home = get_page_by_title('Home');//get_page_by_path('/home', OBJECT, 'page');
  $mapa_url = CFS()->get( 'url_mapa', $page_home->ID, array( 'format' => 'api' ));
  $url_instagram = CFS()->get( 'url_instagram', $page_home->ID, array( 'format' => 'api' ));
  $direccion_principal = CFS()->get( 'direccion_principal', $page_home->ID, array( 'format' => 'api' ));
  $correo_principal = CFS()->get( 'correo_principal', $page_home->ID, array( 'format' => 'api' ));
  $correo_secundario = CFS()->get( 'correo_secundario', $page_home->ID, array( 'format' => 'api' ));
  $telefono_principal = CFS()->get( 'telefono_principal', $page_home->ID, array( 'format' => 'api' ));

  $texto_intro_pagina_contacto = CFS()->get( 'texto_intro_pagina_contacto', $post->ID, array( 'format' => 'api' ));
  $texto_formulario_contacto = CFS()->get( 'texto_formulario_contacto', $post->ID, array( 'format' => 'api' ));
  $correos_formulario = CFS()->get( 'correos_formulario', $post->ID, array( 'format' => 'api' ));

?>
<?php
  //print_r($correos_formulario);die;
?>
<?php include get_theme_file_path( 'template-parts/page-tile.php' );  ?>
<section id="contact" class="bg-dark-f4">
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-lg-6">
              <?php echo $texto_intro_pagina_contacto; ?>
              <?php if ( $telefono_principal != '' ) { ?>
              <div class="icon-box icon-left iconbox-centered-in-responsive iconbox-theme-colored1 animate-icon-on-hover animate-icon-rotate mb-50">
                <div class="icon-box-wrapper">
                  <div class="icon-wrapper">
                    <a class="icon icon-type-font-icon icon-dark icon-circled"> <i class="flaticon-contact-044-call-1"></i> </a>
                  </div>
                  <div class="icon-text">
                    <h5 class="icon-box-title mt-0">Teléfono</h5>
                    <div class="content"><a href="tel:<?php echo $telefono_principal; ?>"><?php echo $telefono_principal; ?></a></div>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
              <?php } ?>
              <?php if ( $correo_principal != '' ) { ?>
              <div class="icon-box icon-left iconbox-centered-in-responsive iconbox-theme-colored1 animate-icon-on-hover animate-icon-rotate mb-50">
                <div class="icon-box-wrapper">
                  <div class="icon-wrapper">
                    <a class="icon icon-type-font-icon icon-dark icon-circled"> <i class="flaticon-contact-043-email-1"></i> </a>
                  </div>
                  <div class="icon-text">
                    <h5 class="icon-box-title mt-0">Email</h5>
                    <div class="content"><a href="mailto:<?php echo $correo_principal; ?>"><?php echo $correo_principal; ?></a></div>
                    <div class="content"><a href="mailto:<?php echo $correo_secundario; ?>"><?php echo $correo_secundario; ?></a></div>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
              <?php } ?>
              <?php if ( $direccion_principal != '' ) { ?>
              <div class="icon-box icon-left iconbox-centered-in-responsive iconbox-theme-colored1 animate-icon-on-hover animate-icon-rotate mb-50">
                <div class="icon-box-wrapper">
                  <div class="icon-wrapper">
                    <a class="icon icon-type-font-icon icon-dark icon-circled"> <i class="flaticon-contact-025-world"></i> </a>
                  </div>
                  <div class="icon-text">
                    <h5 class="icon-box-title mt-0">Dirección</h5>
                    <div class="content"><?php echo $direccion_principal; ?></div>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
              <?php } ?>
              <?php if ( $url_instagram != '' ) { ?>
              <div class="icon-box icon-left iconbox-centered-in-responsive icon-instagram animate-icon-on-hover animate-icon-rotate mb-50">
                <div class="icon-box-wrapper">
                  <div class="icon-wrapper">
                    <a class="icon icon-type-font-icon icon-dark icon-circled"> <i class="fab fa-instagram"></i> </a>
                  </div>
                  <div class="icon-text">
                    <h5 class="icon-box-title mt-0">Instagram</h5>
                    <div class="content">
                      <a href="<?php echo $url_instagram; ?>" target="_blank">
                        <?php echo $url_instagram; ?>  
                      </a>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
              <?php } ?>
            </div>
            <div class="col-lg-6">
              <?php echo $texto_formulario_contacto; ?>
              <!-- Contact Form -->
              <form id="contact_form" name="contact_form" class="" action="<?php echo get_template_directory_uri(); ?>/includes/sendmail-without-phpmailer.php" method="post" novalidate="novalidate">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="mb-3">
                      <label>Nombre <small>*</small></label>
                      <input name="form_name" class="form-control required" type="text" placeholder="Ingresa Nombre">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="mb-3">
                      <label>Email <small>*</small></label>
                      <input name="form_email" class="form-control required email" type="email" placeholder="Ingresa Email">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <div class="mb-3">
                      <label>Asunto <small>*</small></label>
                      <input name="form_subject" class="form-control required" type="text" placeholder="Ingresa Asunto">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="mb-3">
                      <label>Teléfono</label>
                      <input name="form_phone" class="form-control" type="text" placeholder="Ingresa teléfono">
                    </div>
                  </div>
                </div>

                <div class="mb-3">
                  <label>Mensaje</label>
                  <textarea name="form_message" class="form-control required" rows="5" placeholder="Ingresa el mensaje"></textarea>
                </div>
                <div class="mb-3">
                  <input name="form_botcheck" class="form-control" type="hidden" value="">
                  <button type="submit" class="btn btn-flat btn-theme-colored1 text-uppercase mt-10 mb-sm-30 border-left-theme-color-2-4px" data-loading-text="Enviando...">Enviar</button>
                  <button type="reset" class="btn btn-flat btn-theme-colored3 text-uppercase mt-10 mb-sm-30 border-left-theme-color-2-4px">Limpiar</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>


    <!-- -->
    <section class="d-none">
      <div class="container p-0 pb-5">
        <div class="row">
          <div class="col-md-12">
          <iframe src="<?php echo $mapa_url; ?>"  data-tm-width="100%" allowfullscreen="" style="width: 100%;" height="600"></iframe>
          </div>
        </div>
      </div>
    </section>
</div>

