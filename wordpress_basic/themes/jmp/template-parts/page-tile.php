<?php 
    if ( is_category() ) {
        $title = single_cat_title( '', false );
    } elseif ( is_tag() ) {
        $title = single_tag_title( '', false );
    } elseif ( is_author() ) {
        $title = '<span class="vcard">' . get_the_author() . '</span>';
    } elseif ( is_post_type_archive() ) {
        $title = post_type_archive_title( '', false );
    } elseif ( is_tax() ) {
        $title = single_term_title( '', false );
    } elseif ( is_page() ) {
        $title = single_post_title( '', false );
    }
?>
<section class="page-title tm-page-title page-title-standard layer-overlay overlay-dark-9 bg-img-right2" 
    data-tm-bg-img="<?php echo get_template_directory_uri(); ?>/assets/images/bg/bg-01.png">
    <div class="container padding-small">
    <div class="row">
        <div class="col-md-12 title-content text-center">
        <h2 class="title text-warn"><?php echo $title; ?></h2>
        <nav class="breadcrumbs" role="navigation" aria-label="Breadcrumbs">
            <div class="breadcrumbs">
            <span><a href="<?php echo get_home_url(); ?>" rel="home">Home</a></span>
            <span><i class="fa fa-angle-right"></i></span>
            <span class="active"><?php echo $title; ?></span>
            </div>
        </nav>
        </div>
    </div>
    </div>
</section>