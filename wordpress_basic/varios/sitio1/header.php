<!-- Header -->
<header id="header" class="header header-layout-type-header-2rows">
    <div class="header-top">
      <div class="container">
        <div class="row">
          <div class="col-xl-auto header-top-left align-self-center text-center text-xl-start">
            <ul class="element contact-info">
              <li class="contact-phone text-dark"><i class="fa fa-phone font-icon sm-display-block"></i> Tel: (+56) 2 2587 1900</li>
              <li class="contact-email text-dark"><i class="fa fa-envelope font-icon sm-display-block"></i> contacto@fundicionlasrosas.cl</li>
              <li class="contact-address text-dark"><i class="fa fa-map font-icon sm-display-block"></i>   Av. Balmaceda 2140, Malloco</li>
            </ul>
          </div>
          <div class="col-xl-auto ms-xl-auto header-top-right align-self-center text-center text-xl-end">
            <div class="element pt-0 pb-0">
              <ul class="styled-icons icon-dark icon-theme-colored icon-circled clearfix">
                <li><a class="social-link" target="_blank" href="https://www.linkedin.com/in/fundicion-las-rosas-7720a5208" ><i class="fab fa-linkedin text-white"></i></a></li>
              </ul>
            </div>
            <div style="display:none;" class="element pt-0 pt-lg-10 pb-0">
              <a href="ajax-load/form-appointment.html" class="btn btn-theme-colored2 btn-sm ajaxload-popup">Make an Appointment</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="header-nav tm-enable-navbar-hide-on-scroll">
      <div class="header-nav-wrapper navbar-scrolltofixed">
        <div class="menuzord-container header-nav-container">
          <div class="container position-relative">
            <div class="row header-nav-col-row">
              <div class="col-sm-auto align-self-center">
                <a class="menuzord-brand site-brand" href="/index.php">
                  <img class="logo-default logo-1x" src="images/logo_flat_orange.png" alt="Logo">
                  <img class="logo-default logo-2x retina" src="images/logo_flat_orange.png" alt="Logo">
                  <span class="logo-txt">
                    Fundición Las Rosas
                  </span>
                </a>
              </div>
              <div class="col-sm-auto ms-auto pr-0 align-self-center">
                <?php include_once("./menu.php") ?>
              </div>
            </div>
            <div class="row d-block d-xl-none">
               <div class="col-12">
                <nav id="top-primary-nav-clone" class="menuzord d-block d-xl-none default menuzord-color-default menuzord-border-boxed menuzord-responsive" data-effect="slide" data-animation="none" data-align="right">
                 <ul id="main-nav-clone" class="menuzord-menu menuzord-right menuzord-indented scrollable">
                 </ul>
                </nav>
               </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>