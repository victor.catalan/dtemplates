<section class="page-title tm-page-title page-title-standard parallaxr layer-overlay bg-img-center" data-tm-bg-img="images/bg/metalurgia.jpg" style="background-image: url(&quot;images/bg/metalurgia.jpg&quot;);">
    <div class="container pt-80 pb-80">
        <div class="row">
            <div class="col-md-8 title-content sm-text-center">
                <h3 class="title"><?php echo $titulo; ?></h3>
            </div>
            <div class="col-md-4 title-content text-center">
                <nav class="breadcrumbs" role="navigation" aria-label="Breadcrumbs">
                    <div class="breadcrumbs">
                        <span><a href="/" rel="home">Home</a></span>
                        <span><i class="fa fa-angle-right"></i></span>
                        <span class="active"><?php echo $titulo; ?></span>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</section>