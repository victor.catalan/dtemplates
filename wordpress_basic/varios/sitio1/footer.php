<!-- Footer -->
<footer id="footer" class="footer footer-currenty-style bg-size-cover bg-no-repeat">
    <div class="footer-widget-area">
      <div class="footer-top-part">
        <div class="container pt-100 pb-70">
          <div class="row footer-bottom-border pb-60 mb-60 align-items-center text-center text-md-start">
            <div class="col-md-6 col-lg-6 col-xl-7">
              <img class="mb-sm-30" src="images/logo_flat_grey.png" style="width: 100px;" alt="">
            </div>
            <div class="col-md-6 col-lg-6 col-xl-5">
              <div class="widget widget-newsletter m-0">
                <form id="mailchimp-subscription-form10" class="newsletter-form cp-newsletter">
                  <div class="input-group">
                    <input type="email" id="mce-EMAIL" class="form-control" placeholder="Correo electrónico" name="EMAIL" value="">
                    <div class="input-group-append tm-sc tm-sc-button">
                      <button type="submit" class="btn btn-theme-colored1 btn-sm form-control"><i class="fa fa-arrow-right"></i></button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-3">
              <div class="widget">
                <h4 class="widget-title">Acerca de nosotros</h4>
                <p>Lorem ipsum dolor sit amet, consect etur adi pisicing elit sed do eiusmod tempor incididunt ut labore.</p>
                <ul class="styled-icons icon-md icon-gray icon-circled icon-theme-colored2">
                  <li><a class="social-link" target="_blank" href="https://www.linkedin.com/in/fundicion-las-rosas-7720a5208"><i class="fab fa-linkedin"></i></a></li>
                </ul>
              </div>
            </div>
            <div class="col-md-3">
              <div class="widget widget_nav_menu">
                <h4 class="widget-title">Productos</h4>
                <div class="menu-service-nav-menu-container">
                  <ul class="menu">
                    <li class="menu-item"><a href="/productos.php">Fundición</a></li>
                    <li class="menu-item"><a href="/productos.php">Maestranza</a></li>
                    <li class="menu-item"><a href="/productos.php">Muebleria</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="widget widget_nav_menu">
                <h4 class="widget-title">Capacidades</h4>
                <div class="menu-footer-page-list">
                  <ul id="footer-page-list" class="menu">
                    <li><a href="/capacidades.php">Hornos</a></li>
                    <li><a href="/capacidades.php">Piezas</a></li>
                    <li><a href="/capacidades.php">Cifras Claves</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="widget widget_nav_menu">
                <h4 class="widget-title">Servicios</h4>
                <div class="menu-footer-page-list">
                  <ul id="footer-page-list" class="menu">
                    <li><a href="/servicios.php">Fundición</a></li>
                    <li><a href="/servicios.php">Maestranza</a></li>
                    <li><a href="/servicios.php">Mueblería</a></li>
                  </ul>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>
      <div class="footer-bottom">
        <div class="container">
          <div class="row pt-40 pb-40 justify-content-center">
            <div class="col-sm-6">
              <div class="footer-paragraph text-center">
                © 2021 Fundición Las Rosas
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>