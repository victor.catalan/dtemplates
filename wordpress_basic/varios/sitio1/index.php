<?php include_once("./head.php") ?>
<?php include_once("./side_panel.php") ?>

<div id="wrapper" class="clearfix">
  <?php include_once("./header.php") ?>  
  
  <div class="main-content-area">

    <!-- Section: home Start -->
    <section id="home">
      <div class="container-fluid p-0">
        <div class="row">
          <div class="col">
            <!-- START Fundición Las Rosas HTML RevSlider REVOLUTION SLIDER 6.2.19 -->
            <p class="rs-p-wp-fix"></p>
            <rs-module-wrap id="rev_slider_1_1_wrapper" data-alias="Fundición Las Rosas-html-revslider" data-source="gallery" style="background:transparent;padding:0;margin:0px auto;margin-top:0;margin-bottom:0;">
              <rs-module id="rev_slider_1_1" style="" data-version="6.2.19">
                <rs-slides>
                  <rs-slide data-key="rs-42" data-title="Slide 1" data-thumb="images/bg/slide1.jpg" data-anim="ei:d;eo:d;s:d;r:0;t:slotslide-horizontal;sl:d;">
                    <img src="images/bg/slide1.jpg" title="b" width="1920" height="1280" data-parallax="off" class="rev-slidebg" data-no-retina>
                    <!--
                      -->
                    <rs-layer
                      id="slider-14-slide-42-layer-9"
                      data-type="shape"
                      data-rsp_ch="on"
                      data-xy="xo:312px,214px,126px,62px;yo:255px,238px,230px,224px;"
                      data-text="w:normal;s:20,15,11,6;l:0,19,14,8;"
                      data-dim="w:90px,70px,65px,50px;h:90px,70px,65px,50px;"
                      data-border="bor:70px,70px,70px,70px;"
                      data-frame_1="st:1520;sp:1000;sR:1520;"
                      data-frame_999="o:0;st:w;sR:6480;"
                      style="z-index:9;background-color:#ff4f00;"
                      >
                    </rs-layer>
                    <!--
                      -->
                    <rs-layer
                      id="slider-14-slide-42-layer-10"
                      data-type="text"
                      data-rsp_ch="on"
                      data-xy="x:c;yo:194px,178px,174px,171px;"
                      data-text="w:normal;s:24,22,20,18;l:30,28,24,25;fw:500;a:center;"
                      data-dim="w:427px,406px,352px,354px;"
                      data-frame_1="st:500;sp:1000;sR:500;"
                      data-frame_999="o:0;st:w;sR:7500;"
                      class="font-current-theme1"
                      style="z-index:11;ext-transform:capitalize;"
                      >Servicios de alta calidad
                    </rs-layer>
                    <!--
                      -->
                    <rs-layer
                      id="slider-14-slide-42-layer-18"
                      data-type="text"
                      data-rsp_ch="on"
                      data-xy="x:c;yo:258px,239px,225px,223px;"
                      data-text="w:normal;s:80,72,64,42;l:105,85,86,64;ls:0px,0px,-1px,0px;fw:800;a:center;"
                      data-dim="w:auto,auto,auto,455px;"
                      data-frame_1="st:1100;sp:1000;sR:1100;"
                      data-frame_999="o:0;st:w;sR:6900;"
                      class="font-current-theme1"
                      style="z-index:10;"
                      >Fundición con años de tradición
                    </rs-layer>
                    <!--
                      -->
                    <rs-layer
                      id="slider-14-slide-42-layer-22"
                      data-type="text"
                      data-rsp_ch="on"
                      data-xy="x:c;xo:0,0,0,-8px;yo:528px,470px,440px,396px;"
                      data-text="w:normal;s:20,16,16,15;l:22,20,18,20;a:center;"
                      data-frame_1="st:2010;sp:1000;sR:2010;"
                      data-frame_999="o:0;st:w;sR:5990;"
                      class="font-current-theme1"
                      style="z-index:8;"
                      ><a href="page-about.html" class="btn btn-lg btn-bordered btn-theme-colored1 text-white">Ver más</a>
                    </rs-layer>
                    <!--
                      -->
                    <rs-layer
                      id="slider-14-slide-42-layer-33"
                      data-type="shape"
                      data-rsp_ch="on"
                      data-text="w:normal;s:20,15,11,6;l:0,19,14,8;"
                      data-dim="w:100%;h:100%;"
                      data-basealign="slide"
                      data-frame_1="st:500;sp:1000;"
                      data-frame_999="o:0;st:w;sR:8000;"
                      style="z-index:3;background-color:rgba(0,0,0,0.5);"
                    >
                    </rs-layer>
                  </rs-slide>
                  <rs-slide data-key="rs-45" data-title="Slide 1" data-thumb="images/bg/slide2.jpg" data-anim="ei:d;eo:d;s:d;r:0;t:slotslide-horizontal;sl:d;">
                    <img src="images/bg/slide2.jpg" title="b" width="1920" height="1280" data-parallax="off" class="rev-slidebg" data-no-retina>
                    <!--
                      -->
                    <rs-layer
                      id="slider-14-slide-45-layer-9"
                      data-type="shape"
                      data-rsp_ch="on"
                      data-xy="xo:312px,214px,126px,62px;yo:255px,238px,230px,224px;"
                      data-text="w:normal;s:20,15,11,6;l:0,19,14,8;"
                      data-dim="w:90px,70px,65px,50px;h:90px,70px,65px,50px;"
                      data-border="bor:70px,70px,70px,70px;"
                      data-frame_1="st:1520;sp:1000;sR:1520;"
                      data-frame_999="o:0;st:w;sR:6480;"
                      style="z-index:9;background-color:#ff4f00;"
                      >
                    </rs-layer>
                    <!--
                      -->
                    <rs-layer
                      id="slider-14-slide-45-layer-10"
                      data-type="text"
                      data-rsp_ch="on"
                      data-xy="x:c;yo:194px,178px,174px,171px;"
                      data-text="w:normal;s:24,22,20,18;l:30,28,24,25;fw:500;a:center;"
                      data-dim="w:427px,406px,352px,354px;"
                      data-frame_1="st:500;sp:1000;sR:500;"
                      data-frame_999="o:0;st:w;sR:7500;"
                      class="font-current-theme1"
                      style="z-index:11;ext-transform:capitalize;"
                      >Mejores materiales
                    </rs-layer>
                    <!--
                      -->
                    <rs-layer
                      id="slider-14-slide-45-layer-18"
                      data-type="text"
                      data-rsp_ch="on"
                      data-xy="x:c;yo:258px,239px,225px,223px;"
                      data-text="w:normal;s:80,72,64,42;l:105,85,86,64;ls:0px,0px,-1px,0px;fw:800;a:center;"
                      data-dim="w:auto,auto,auto,455px;"
                      data-frame_1="st:1100;sp:1000;sR:1100;"
                      data-frame_999="o:0;st:w;sR:6900;"
                      class="font-current-theme1"
                      style="z-index:10;"
                      >Productos de <br>
                      máxima calidad
                    </rs-layer>
                    <!--
                      -->
                    <rs-layer
                      id="slider-14-slide-45-layer-22"
                      data-type="text"
                      data-rsp_ch="on"
                      data-xy="x:c;xo:0,0,0,-8px;yo:528px,470px,440px,396px;"
                      data-text="w:normal;s:20,16,16,15;l:22,20,18,20;a:center;"
                      data-frame_1="st:2010;sp:1000;sR:2010;"
                      data-frame_999="o:0;st:w;sR:5990;"
                      class="font-current-theme1"
                      style="z-index:8;"
                      ><a href="page-about.html" class="btn btn-lg btn-bordered btn-theme-colored1 text-white">Ver más</a>
                    </rs-layer>
                    <!--
                      -->
                    <rs-layer
                      id="slider-14-slide-45-layer-33"
                      data-type="shape"
                      data-rsp_ch="on"
                      data-text="w:normal;s:20,15,11,6;l:0,19,14,8;"
                      data-dim="w:100%;h:100%;"
                      data-basealign="slide"
                      data-frame_1="st:500;sp:1000;"
                      data-frame_999="o:0;st:w;sR:8000;"
                      style="z-index:3;background-color:rgba(0,0,0,0.5);"
                    >
                    </rs-layer>
                  </rs-slide>
                  <rs-slide data-key="rs-46" data-title="Slide 1" data-thumb="images/bg/slide3.jpg" data-anim="ei:d;eo:d;s:d;r:0;t:slotslide-horizontal;sl:d;">
                    <img src="images/bg/slide3.jpg" title="b" width="1920" height="1280" data-parallax="off" class="rev-slidebg" data-no-retina>
                    <!--
                      -->
                    <rs-layer
                      id="slider-14-slide-46-layer-9"
                      data-type="shape"
                      data-rsp_ch="on"
                      data-xy="xo:312px,214px,126px,62px;yo:255px,238px,230px,224px;"
                      data-text="w:normal;s:20,15,11,6;l:0,19,14,8;"
                      data-dim="w:90px,70px,65px,50px;h:90px,70px,65px,50px;"
                      data-border="bor:70px,70px,70px,70px;"
                      data-frame_1="st:1520;sp:1000;sR:1520;"
                      data-frame_999="o:0;st:w;sR:6480;"
                      style="z-index:9;background-color:#ff4f00;"
                      >
                    </rs-layer>
                    <!--
                      -->
                    <rs-layer
                      id="slider-14-slide-46-layer-22"
                      data-type="text"
                      data-rsp_ch="on"
                      data-xy="x:c;xo:0,0,0,-8px;yo:528px,470px,440px,396px;"
                      data-text="w:normal;s:20,16,16,15;l:22,20,18,20;a:center;"
                      data-frame_1="st:2010;sp:1000;sR:2010;"
                      data-frame_999="o:0;st:w;sR:5990;"
                      class="font-current-theme1"
                      style="z-index:8;"
                      ><a href="page-about.html" class="btn btn-lg btn-bordered btn-theme-colored1 text-white">Ver más</a>
                    </rs-layer>
                    <!--
                      -->
                    <rs-layer
                      id="slider-14-slide-46-layer-33"
                      data-type="shape"
                      data-rsp_ch="on"
                      data-text="w:normal;s:20,15,11,6;l:0,19,14,8;"
                      data-dim="w:100%;h:100%;"
                      data-basealign="slide"
                      data-frame_1="st:500;sp:1000;"
                      data-frame_999="o:0;st:w;sR:8000;"
                      style="z-index:3;background-color:rgba(0,0,0,0.5);"
                    >
                    </rs-layer>
                  </rs-slide>
                </rs-slides>
                <rs-static-layers>
                  <!--
                    -->
                </rs-static-layers>
              </rs-module>
              <script type="text/javascript">
                if(typeof revslider_showDoubleJqueryError === "undefined") {
                  function revslider_showDoubleJqueryError(sliderID) {
                    var err = "<div class='rs_error_message_box'>";
                    err += "<div class='rs_error_message_oops'>Oops...</div>";
                    err += "<div class='rs_error_message_content'>";
                    err += "You have some jquery.js library include that comes after the Slider Revolution files js inclusion.<br>";
                    err += "To fix this, you can:<br>&nbsp;&nbsp;&nbsp; 1. Set 'Module General Options' -> 'Advanced' -> 'jQuery & OutPut Filters' -> 'Put JS to Body' to on";
                    err += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jQuery.js inclusion and remove it";
                    err += "</div>";
                  err += "</div>";
                    var slider = document.getElementById(sliderID); slider.innerHTML = err; slider.style.display = "block";
                  }
                }
              </script>
            </rs-module-wrap>
            <!-- END REVOLUTION SLIDER -->
          </div>
        </div>
      </div>
    </section>
    <!-- Section: home End -->

    <!-- Section: Features -->
    <section style="display:none;" class="features-section bg-lighter">
      <div class="container-fluid pt-30 pb--0">
        <div class="section-content">
          <div class="row">
            <div class="col-lg-6 col-xl-4">
              <div class="feature-current-item mb-lg-30 bg-dark">
                <div class="inner-box">
                  <h6 class="feature-sub-title text-uppercase">Our Features</h6>
                  <h4 class="feature-title">We Always Building Quality Industrial</h4>
                </div>
                <div class="feature-icon">
                  <i class="flaticon-contact-009-phone-3"></i>
                </div>
              </div>
            </div>
            <div class="col-lg-6 col-xl-4">
              <div class="feature-current-item mb-lg-30 bg-dark">
                <div class="inner-box">
                  <h6 class="feature-sub-title text-uppercase">Our Features</h6>
                  <h4 class="feature-title">Best Manufacturing Service Provider</h4>
                </div>
                <div class="feature-icon">
                  <i class="flaticon-contact-024-globe"></i>
                </div>
              </div>
            </div>
            <div class="col-lg-6 col-xl-4">
              <div class="feature-current-item bg-dark">
                <div class="inner-box">
                  <h6 class="feature-sub-title text-uppercase">Our Features</h6>
                  <h4 class="feature-title">Experience Trusted Best Contractors</h4>
                </div>
                <div class="feature-icon">
                  <i class="flaticon-contact-011-web"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Section: About -->
    <section style="display:none;" class="about-section bg-lighter">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-lg-9 col-xl-5">
            <div class="image-box">
              <img src="images/about/about1.jpg" alt="">
              <div class="call-anytime-box bg-dark">
                <div class="icon-box">
                  <i class="fa fa-phone"></i>
                </div>
                <div class="number">
                  <p class="mb-0">Call Anytime</p>
                  <h5 class="m-0">92 000 666 8888</h5>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12 col-lg-12 col-xl-7 mt-lg-50 pl-60 pl-lg--0 pl-sm-15">
            <h6 class="mt-0 about-sub-title text-uppercase sub-title-side-line">About Compnay</h6>
            <h2>Your Partner for Future Innovation</h2>
            <p class="lead font-italic text-theme-colored1 mb-20">There are many variations of passages of lorem ipsum available but the majority have suffered</p>
            <div class="row">
              <div class="col-lg-6 col-md-6 col-xl-7">
                <div class="tm-sc tm-sc-unordered-list list-style5 mb-40">
                  <ul>
                    <li>If you are going to use a pass</li>
                    <li>Lorem ipsum available</li>
                    <li>The majority have suffered</li>
                    <li>Totam rem aperiam eaque ipsa</li>
                    <li>Sed ut perspiciatis</li>
                  </ul>
                </div>
                <a href="page-about.html" class="current-btn-style mb-sm-40">Ver más</a>
              </div>
              <div class="col-lg-6 col-md-6 col-xl-5">
                <div class="pie-chart-box bg-dark">
                  <div class="tm-sc tm-sc-pie-chart pie-chart-current-style1">
                    <div class="pie-chart"
                      data-bar-color="#ff4f00"
                      data-track-color="#eee"
                      data-scale-color="#ff4f00"
                      data-scale-length="0"
                      data-line-cap="round"
                      data-line-width="6"
                      data-size="140"
                      data-percent="90">
                      <span class="percent"></span>
                    </div>
                  </div>
                  <h5 class="mb-0">Clients Satisfied</h5>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Section: Service -->
    <section style="display:none;" class="layer-overlay overlay-theme-colored2-9" data-tm-bg-img="images/bg/bg2.jpg">
      <div class="container" data-tm-padding-bottom="320px">
        <div class="section-title">
          <div class="row">
            <div class="col-lg-12">
              <div class="tm-sc-section-title section-title text-center">
                <div class="title-wrapper">
                  <h6 class="mt-0 mb-20 about-sub-title text-uppercase sub-title-side-line">The Best Services</h6>
                  <h2 class="title text-white">Services We’re Offering</h2>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section style="display:none;" class="bg-lighter">
      <div class="container pt-0">
        <div class="section-content">
          <div class="row">
            <div class="col-md-6 col-xl-4">
              <div class="tm-sc-services services-style-current-theme" data-tm-margin-top="-319px">
                <div class="tm-service">
                  <div class="thumb">
                    <img class="w-100" src="images/service/1.jpg" alt="Image">
                  </div>
                  <div class="details clearfix bg-dark">
                    <h4 class="title"><a href="page-service-details.html">Metal Industry</a></h4>
                    <p>Lorem ipsum is simply free text dolor sit am adipi we help you is in the right jobs sicing elit.</p>
                    <a href="page-service-details.html" class="services-btn-style float-end">Read More</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-xl-4">
              <div class="tm-sc-services services-style-current-theme mt-sm-30" data-tm-margin-top="-319px">
                <div class="tm-service">
                  <div class="thumb">
                    <img class="w-100" src="images/service/2.jpg" alt="Image">
                  </div>
                  <div class="details clearfix bg-dark">
                    <h4 class="title"><a href="page-service-details.html">Civil Engineering</a></h4>
                    <p>Lorem ipsum is simply free text dolor sit am adipi we help you is in the right jobs sicing elit.</p>
                    <a href="page-service-details.html" class="services-btn-style float-end">Read More</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-xl-4">
              <div class="tm-sc-services services-style-current-theme mt-lg-30" data-tm-margin-top="-319px">
                <div class="tm-service">
                  <div class="thumb">
                    <img class="w-100" src="images/service/3.jpg" alt="Image">
                  </div>
                  <div class="details clearfix bg-dark">
                    <h4 class="title"><a href="page-service-details.html">Petrol & Gas</a></h4>
                    <p>Lorem ipsum is simply free text dolor sit am adipi we help you is in the right jobs sicing elit.</p>
                    <a href="page-service-details.html" class="services-btn-style float-end">Read More</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Section: Testimonials -->
    <section style="display:none;" class="bg-lighter position-relative overflow-hidden">
      <div class="container pt-0 pb-80">
        <div class="section-content">
          <div class="row">
            <div class="col-md-6 col-xl-6">
              <div class="tm-sc-section-title section-title">
                <div class="title-wrapper">
                  <h6 class="mt-0 mb-20 about-sub-title text-uppercase sub-title-side-line">Our services</h6>
                  <h2 class="title">What They're Saying</h2>
                </div>
              </div>
              <div class="tm-sc-testimonials tm-sc-testimonials-carousel owl-dots-light-skin owl-dots-center testimonial-has-quote-icon mb-sm-30">
                <div class="slick-slider-thumbs thumbs-left-aligned slider-nav-slick11"  data-slider-id="slick11">
                  <div class="slider-item">
                    <img width="128" height="128" src="images/testimonials/1.jpg" class="img-thumbnail rounded-circle" alt=""/>
                  </div>
                  <div class="slider-item">
                    <img width="128" height="128" src="images/testimonials/2.jpg" class="img-thumbnail rounded-circle" alt=""/>
                  </div>
                  <div class="slider-item">
                    <img width="128" height="128" src="images/testimonials/3.jpg" class="img-thumbnail rounded-circle" alt=""/>
                  </div>
                </div>
                <!-- Isotope Gallery Grid -->
                <div id="slick11" class="slick-thumbnail-slider" data-nav="true" data-slider-id="slick11">
                  <div class="tm-carousel-item">
                    <div class="tm-testimonial">
                      <div class="testimonial-inner">
                        <div class="testimonial-text-holder">
                          <div class="author-text">A typical business contents insurance policy will cover damage or loss to furniture, tools and equipment as a result of a fire, flood or theft. This type of cover is not mandatory under UK law.</div>
                        </div>
                        <div class="testimonial-author-details">
                          <div class="testimonial-author-info-holder">
                            <h5 class="name">Jim Grace</h5>
                            <span class="job-position">- Co-Founder</span> <a class="company-url" href="#"></a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="tm-carousel-item">
                    <div class="tm-testimonial">
                      <div class="testimonial-inner">
                        <div class="testimonial-text-holder">
                          <div class="author-text">A typical business contents insurance policy will cover damage or loss to furniture, tools and equipment as a result of a fire, flood or theft. This type of cover is not mandatory under UK law.</div>
                        </div>
                        <div class="testimonial-author-details">
                          <div class="testimonial-author-info-holder">
                            <h5 class="name">Katrina Grace</h5>
                            <span class="job-position">- Engineer</span> <a class="company-url" href="#"></a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="tm-carousel-item">
                    <div class="tm-testimonial">
                      <div class="testimonial-inner">
                        <div class="testimonial-text-holder">
                          <div class="author-text">A typical business contents insurance policy will cover damage or loss to furniture, tools and equipment as a result of a fire, flood or theft. This type of cover is not mandatory under UK law.</div>
                        </div>
                        <div class="testimonial-author-details">
                          <div class="testimonial-author-info-holder">
                            <h5 class="name">Zhon Done</h5>
                            <span class="job-position">- Engineer</span> <a class="company-url" href="#"></a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-xl-6">
              <div class="funfact-current-item-style1 mt-70 mb-15 bg-dark">
                <div class="funfact-icon">
                  <i class="flaticon-contact-009-phone-3"></i>
                </div>
                <div class="funfact-detail-box">
                  <h2 class="counter">
                    <span class="animate-number" data-value="9640" data-animation-duration="1500">0</span>
                  </h2>
                  <h6 class="title text-gray mt-0">Project Completed</h6>
                </div>
              </div>
              <div class="funfact-current-item-style1 mb-15 bg-dark">
                <div class="funfact-icon">
                  <i class="flaticon-contact-024-globe"></i>
                </div>
                <div class="funfact-detail-box">
                  <h2 class="counter">
                    <span class="animate-number" data-value="7250" data-animation-duration="1500">0</span>
                  </h2>
                  <h6 class="title text-gray mt-0">Satisfied Customers</h6>
                </div>
              </div>
              <div class="funfact-current-item-style1 mb-15 bg-dark">
                <div class="funfact-icon">
                  <i class="flaticon-contact-031-time-call-1"></i>
                </div>
                <div class="funfact-detail-box">
                  <h2 class="counter">
                    <span class="animate-number" data-value="6540" data-animation-duration="1500">0</span>
                  </h2>
                  <h6 class="title text-gray mt-0">Skilled Contractors</h6>
                </div>
              </div>
              <div class="layer-image-currenty-style1 layer-image-wrapper">
                <div class="layer-image">
                  <img src="images/bg/testimonial-bg1.jpg" alt="Image">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Section: Projects Title -->
    <section>
      <div class="container pb-0">
        <div class="tm-sc-section-title section-title">
          <div class="row align-items-center justify-content-between">
            <div class="col-xl-5 col-lg-6">
              <div class="title-wrapper mb-0">
                <h6 class="subtitle text-uppercase sub-title-side-line">Productos</h6>
                <h2 class="title mb-xl-0 mb-lg-0">Revisa nuestros productos</h2>
              </div>
            </div>
            <div class="col-lg-5">
              <p class="mb-0">Lorem ipsum dolor sit am adipi we help you ensure everyone is in the right jobs sicing elit, sed do consulting firms Et leggings across the nation tempor.</p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Section: Projects -->
    <section>
      <div class="container-fluid pt-0 pb-0">
        <div class="section-content">
          <div class="row">
            <div class="col-xl-12">
              <div class="owl-carousel owl-theme tm-owl-carousel-4col" data-autoplay="true" data-loop="true" data-duration="6000" data-smartspeed="300" data-margin="10" data-stagepadding="0" data-laptop="3">
                <div class="item">
                  <div class="tm-sc-projects projects-current-theme">
                    <div class="tm-project">
                      <div class="thumb">
                        <img class="w-100" src="images/projects/1.jpg" alt="Image">
                        <div class="details bg-dark">
                          <h6 class="sub-title"><a href="page-project-details.html">Fundición</a></h6>
                          <h4 class="title"><a href="page-project-details.html">Producto 1</a></h4>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <div class="tm-sc-projects projects-current-theme">
                    <div class="tm-project">
                      <div class="thumb">
                        <img class="w-100" src="images/projects/2.jpg" alt="Image">
                        <div class="details bg-dark">
                          <h6 class="sub-title"><a href="page-project-details.html">Muebleria</a></h6>
                          <h4 class="title"><a href="page-project-details.html">Producto 2</a></h4>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <div class="tm-sc-projects projects-current-theme">
                    <div class="tm-project">
                      <div class="thumb">
                        <img class="w-100" src="images/projects/3.jpg" alt="Image">
                        <div class="details bg-dark">
                          <h6 class="sub-title"><a href="page-project-details.html">Fundición</a></h6>
                          <h4 class="title"><a href="page-project-details.html">Producto 3</a></h4>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <div class="tm-sc-projects projects-current-theme">
                    <div class="tm-project">
                      <div class="thumb">
                        <img class="w-100" src="images/projects/4.jpg" alt="Image">
                        <div class="details bg-dark">
                          <h6 class="sub-title"><a href="page-project-details.html">Maestranza</a></h6>
                          <h4 class="title"><a href="page-project-details.html">Producto 4</a></h4>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <div class="tm-sc-projects projects-current-theme">
                    <div class="tm-project">
                      <div class="thumb">
                        <img class="w-100" src="images/projects/5.jpg" alt="Image">
                        <div class="details bg-dark">
                          <h6 class="sub-title"><a href="page-project-details.html">Muebleria</a></h6>
                          <h4 class="title"><a href="page-project-details.html">Producto 5</a></h4>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <div class="tm-sc-projects projects-current-theme">
                    <div class="tm-project">
                      <div class="thumb">
                        <img class="w-100" src="images/projects/6.jpg" alt="Image">
                        <div class="details bg-dark">
                          <h6 class="sub-title"><a href="page-project-details.html">Maestranza</a></h6>
                          <h4 class="title"><a href="page-project-details.html">Producto 5</a></h4>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Section: Team -->
    <section style="display:none;" class="bg-img-center bg-img-no-repeat layer-overlay overlay-dark-3" data-tm-bg-img="images/bg/contact-bg1.png">
      <div class="container pb-70">
        <div class="tm-sc-section-title section-title">
          <div class="row align-items-center justify-content-between">
            <div class="col-xl-4 col-lg-6">
              <div class="title-wrapper mb-0">
                <h6 class="subtitle text-uppercase sub-title-side-line">Meet our</h6>
                <h2 class="title mb-xl-0 mb-lg-0">Professional Expert Team</h2>
              </div>
            </div>
            <div class="col-lg-5">
              <p class="mb-0">Lorem ipsum dolor sit am adipi we help you ensure everyone is in the right jobs sicing elit, sed do consulting firms Et leggings across the nation tempor.</p>
            </div>
          </div>
        </div>
        <div class="section-content">
          <div class="row">
            <div class="col-md-6 col-lg-6 col-xl-3">
              <div class="team-item mb-30">
                <div class="team-thumb">
                  <img class="img-fullwidth" src="images/team/1.jpg" alt="1.jpg">
                </div>
                <div class="team-content bg-dark">
                  <div class="team-information">
                    <h4 class="team-name mt-0"><a href="page-team-details.html">Jessica Brown</a></h4>
                    <h6 class="designation mb-0 text-gray mt-0 ">Expert Agent</h6>
                  </div>
                  <div class="team-social">
                    <ul class="styled-icons icon-team-list icon-bordered icon-circled">
                      <li><a class="social-link" href="#"><i class="fab fa-twitter"></i></a></li>
                      <li><a class="social-link" href="#"><i class="fab fa-facebook"></i></a></li>
                      <li><a class="social-link" href="#"><i class="fab fa-pinterest"></i></a></li>
                      <li><a class="social-link" href="#"><i class="fab fa-instagram"></i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-3">
              <div class="team-item mb-30">
                <div class="team-thumb">
                  <img class="img-fullwidth" src="images/team/2.jpg" alt="2.jpg">
                </div>
                <div class="team-content bg-dark">
                  <div class="team-information">
                    <h4 class="team-name mt-0"><a href="page-team-details.html">Mike Hardson</a></h4>
                    <h6 class="designation mb-0 text-gray mt-0 ">Expert Agent</h6>
                  </div>
                  <div class="team-social">
                    <ul class="styled-icons icon-team-list icon-bordered icon-circled">
                      <li><a class="social-link" href="#"><i class="fab fa-twitter"></i></a></li>
                      <li><a class="social-link" href="#"><i class="fab fa-facebook"></i></a></li>
                      <li><a class="social-link" href="#"><i class="fab fa-pinterest"></i></a></li>
                      <li><a class="social-link" href="#"><i class="fab fa-instagram"></i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-3">
              <div class="team-item mb-30">
                <div class="team-thumb">
                  <img class="img-fullwidth" src="images/team/3.jpg" alt="1.jpg">
                </div>
                <div class="team-content bg-dark">
                  <div class="team-information">
                    <h4 class="team-name mt-0"><a href="page-team-details.html">Jessica Brown</a></h4>
                    <h6 class="designation mb-0 text-gray mt-0 ">Expert Agent</h6>
                  </div>
                  <div class="team-social">
                    <ul class="styled-icons icon-team-list icon-bordered icon-circled">
                      <li><a class="social-link" href="#"><i class="fab fa-twitter"></i></a></li>
                      <li><a class="social-link" href="#"><i class="fab fa-facebook"></i></a></li>
                      <li><a class="social-link" href="#"><i class="fab fa-pinterest"></i></a></li>
                      <li><a class="social-link" href="#"><i class="fab fa-instagram"></i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-3">
              <div class="team-item mb-30">
                <div class="team-thumb">
                  <img class="img-fullwidth" src="images/team/4.jpg" alt="3.jpg">
                </div>
                <div class="team-content bg-dark">
                  <div class="team-information">
                    <h4 class="team-name mt-0"><a href="page-team-details.html">Sarah Albert</a></h4>
                    <h6 class="designation mb-0 text-gray mt-0 ">Expert Agent</h6>
                  </div>
                  <div class="team-social">
                    <ul class="styled-icons icon-team-list icon-bordered icon-circled">
                      <li><a class="social-link" href="#"><i class="fab fa-twitter"></i></a></li>
                      <li><a class="social-link" href="#"><i class="fab fa-facebook"></i></a></li>
                      <li><a class="social-link" href="#"><i class="fab fa-pinterest"></i></a></li>
                      <li><a class="social-link" href="#"><i class="fab fa-instagram"></i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Team -->

    <!-- Section: News -->
    <section style="display:none;">
      <div class="container">
        <div class="section-title">
          <div class="row justify-content-center">
            <div class="col-lg-6">
              <div class="tm-sc-section-title section-title text-center">
                <div class="title-wrapper">
                  <h6 class="mt-0 about-sub-title text-uppercase sub-title-side-line">From the Blog</h6>
                  <h2 class="title">News & Articles</h2>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="section-content">
          <div class="row justify-content-md-center">
            <div class="col-md-12 col-lg-8 col-xl-6">
              <div class="blog-style1-current-theme">
                <article class="post">
                  <div class="entry-header">
                    <div class="post-thumb">
                      <div class="thumb"> <img class="w-100" src="images/blog/1.jpg" alt="Image"></div>
                    </div>
                    <div class="post-date">
                      <p class="date">25 <br><span>Nov</span></p>
                    </div>
                  </div>
                  <div class="entry-content">
                    <div class="blog-meta mb-20">
                      <span class="admin-type mr-10">
                        <i class="far fa-user-circle text-theme-colored1"></i>
                      Admin
                      </span><br>
                      <span class="comments-type">
                        <i class="far fa-comments text-theme-colored1"></i>
                        2 Comments
                      </span>
                    </div>
                    <h4 class="entry-title mb-30"><a href="news-details.html" rel="bookmark">We are Always Best for Industrial Solution</a></h4>
                    <p>Lorem ipsum dolor sit am adipi we help you ensure everyone is in the right jobs sicing elit, sed do consulting firms Et leggings across the nation tempor...</p>
                    <a href="news-details.html" class="blog-link-btn"><i class="fas fa-arrow-right"></i></a>
                    <div class="clearfix"></div>
                  </div>
                </article>
              </div>
            </div>
            <div class="col-md-12 col-lg-8 col-xl-6">
              <div class="blog-style1-current-theme mt-lg-60">
                <article class="post">
                  <div class="entry-header">
                    <div class="post-thumb">
                      <div class="thumb"> <img class="w-100" src="images/blog/2.jpg" alt="Image"></div>
                    </div>
                    <div class="post-date">
                      <p class="date">25 <br><span>Nov</span></p>
                    </div>
                  </div>
                  <div class="entry-content">
                    <div class="blog-meta mb-20">
                      <span class="admin-type mr-10">
                        <i class="far fa-user-circle text-theme-colored1"></i>
                      Admin
                      </span><br>
                      <span class="comments-type">
                        <i class="far fa-comments text-theme-colored1"></i>
                        2 Comments
                      </span>
                    </div>
                    <h4 class="entry-title mb-20"><a href="news-details.html" rel="bookmark">Industrial & business company future</a></h4>
                    <p>Lorem ipsum dolor sit am adipi we help you ensure everyone is in the right jobs sicing elit, sed do consulting firms Et leggings across the nation tempor...</p>
                    <a href="news-details.html" class="blog-link-btn"><i class="fas fa-arrow-right"></i></a>
                    <div class="clearfix"></div>
                  </div>
                </article>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Start  Divider -->
    <section class="bg-lightest">
      <div class="container pt-30 pb-30">
        <div class="section-content">
          <div class="row">
            <div class="col-sm-12">
              <div class="tm-sc tm-sc-clients tm-sc-clients-carousel">
                <div class="owl-carousel owl-theme tm-owl-carousel-6col" data-autoplay="true" data-loop="true" data-duration="6000" data-smartspeed="300" data-margin="30" data-stagepadding="0" data-laptop="4" data-tablet="3">
                  <div class="item"> <a target="_blank" href="#"> <img src='images/clients/1.png' alt='Image' /> </a></div>
                  <div class="item"> <a target="_blank" href="#"> <img src='images/clients/2.png' alt='Image' /> </a></div>
                  <div class="item"> <a target="_blank" href="#"> <img src='images/clients/3.png' alt='Image' /> </a></div>
                  <div class="item"> <a target="_blank" href="#"> <img src='images/clients/4.png' alt='Image' /> </a></div>
                  <div class="item"> <a target="_blank" href="#"> <img src='images/clients/5.png' alt='Image' /> </a></div>
                  <div class="item"> <a target="_blank" href="#"> <img src='images/clients/6.png' alt='Image' /> </a></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Divider -->

    <!-- Start  Divider -->
    <section class="bg-theme-colored1 overflow-hidden">
      <div class="container pt-0 pb-0">
        <div class="section-content">
          <div class="row">
            <div class="col-xl-6 col-lg-12">
              <div class="mt-80 mb-90 text-center text-xl-start">
                <h2 class="text-white mb-25">¿Buscando un proveedor?</h2>
                <a href="page-project.html" class="btn btn-light btn-md">Ver más</a>
              </div>
            </div>
            <div class="col-xl-6">
              <div class="layer-image-currenty-style2 layer-image-wrapper">
                <div class="layer-image">
                  <img src="images/about/about6.jpg" alt="Image">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Divider -->
  </div>

  <?php include_once("./footer.php") ?>
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
</div>
<?php include_once("./foot.php") ?>