<?php
    $pagina = str_replace(".php", "", $_SERVER['REQUEST_URI']);
    $pagina = str_replace("/", "",$pagina);
?>
<nav id="top-primary-nav" class="menuzord theme-color2" data-effect="slide" data-animation="none" data-align="right">
    <ul id="main-nav" class="menuzord-menu">
    <li class="<?php echo ($pagina=="index")?"active":""; ?> menu-item">
        <a href="/index.php">Home</a>
    </li>
    <li class="<?php echo ($pagina=="quienes")?"active":""; ?> menu-item">
        <a href="/quienes.php">Quienes Somos</a>
    </li>
    <li class="<?php echo ($pagina=="productos")?"active":""; ?> menu-item"><a href="javascript:void(0)">Productos</a>
        <ul class="dropdown">
        <li><a href="/productos.php">Categorías</a></li>
        <li><a href="/productos.php">Tipos</a></li>
        <li><a href="/productos.php">Rubros</a></li>
        </ul>
    </li>
    <li class="<?php echo ($pagina=="servicios")?"active":""; ?> menu-item"><a href="javascript:void(0)">Servicios</a>
        <ul class="dropdown">
        <li><a href="/servicios.php">Fundición</a></li>
        <li><a href="/servicios.php">Maestranza</a></li>
        <li><a href="/servicios.php">Muebleria </a></li>
        </ul>
    </li>
    <li class="<?php echo ($pagina=="capacidades")?"active":""; ?> menu-item"><a href="javascript:void(0)">Capacidades</a>
        <ul class="dropdown">
        <li><a href="/capacidades.php">Hornos</a></li>
        <li><a href="/capacidades.php">Piezas</a></li>
        <li><a href="/capacidades.php">Cifras Claves</a></li>
        </ul>
    </li>
    <li class="<?php echo ($pagina=="contacto")?"active":""; ?> menu-item">
        <a href="/contacto.php">Contacto</a>
    </li>
    </ul>
</nav>