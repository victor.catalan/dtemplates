<?php include_once("./head.php") ?>
<?php include_once("./side_panel.php") ?>

<div id="wrapper" class="clearfix">
  <?php include_once("./header.php") ?>  
  
  <div class="main-content-area">
    <?php 
        $titulo="Contacto";
        include_once("./titulo_pagina.php");
    ?>  
    

    <!-- -->
    <section id="contact" class="bg-dark-f4">
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-lg-6">
              <h5 class="mb-0 text-gray">Estamos felices de ayudarte</h5>
              <h2 class="mb-30">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. </h2>
              <div class="icon-box icon-left iconbox-centered-in-responsive iconbox-theme-colored1 animate-icon-on-hover animate-icon-rotate mb-50">
                <div class="icon-box-wrapper">
                  <div class="icon-wrapper">
                    <a class="icon icon-type-font-icon icon-dark icon-circled"> <i class="flaticon-contact-044-call-1"></i> </a>
                  </div>
                  <div class="icon-text">
                    <h5 class="icon-box-title mt-0">Teléfono</h5>
                    <div class="content"><a href="tel:+123.456.7890">(+56) 2 2587 1900</a></div>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
              <div class="icon-box icon-left iconbox-centered-in-responsive iconbox-theme-colored1 animate-icon-on-hover animate-icon-rotate mb-50">
                <div class="icon-box-wrapper">
                  <div class="icon-wrapper">
                    <a class="icon icon-type-font-icon icon-dark icon-circled"> <i class="flaticon-contact-043-email-1"></i> </a>
                  </div>
                  <div class="icon-text">
                    <h5 class="icon-box-title mt-0">Email</h5>
                    <div class="content"><a href="mailto:needhelp@yourdomain.com">contacto@fundicionlasrosas.cl</a></div>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
              <div class="icon-box icon-left iconbox-centered-in-responsive iconbox-theme-colored1 animate-icon-on-hover animate-icon-rotate mb-50">
                <div class="icon-box-wrapper">
                  <div class="icon-wrapper">
                    <a class="icon icon-type-font-icon icon-dark icon-circled"> <i class="flaticon-contact-025-world"></i> </a>
                  </div>
                  <div class="icon-text">
                    <h5 class="icon-box-title mt-0">Dirección</h5>
                    <div class="content">Av. Balmaceda 2140, Malloco</div>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
              <ul class="styled-icons icon-dark icon-sm icon-circled mt-30">
                <li><a href="https://www.linkedin.com/in/fundicion-las-rosas-7720a5208" data-tm-bg-color="#3B5998" style="background-color: rgb(59, 89, 152) !important;"><i class="fab fa-linkedin"></i></a></li>
              </ul>
            </div>
            <div class="col-lg-6">
              <h2 class="mt-0 mb-0">¿Alguna pregunta?</h2>
              <p class="font-size-20">¡Estamos listo para responderte!</p>
              <!-- Contact Form -->
              <form id="contact_form" name="contact_form" class="" action="includes/sendmail.php" method="post" novalidate="novalidate">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="mb-3">
                      <label>Nombre <small>*</small></label>
                      <input name="form_name" class="form-control" type="text" placeholder="Ingresa Nombre">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="mb-3">
                      <label>Email <small>*</small></label>
                      <input name="form_email" class="form-control required email" type="email" placeholder="Ingresa Email">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <div class="mb-3">
                      <label>Asunto <small>*</small></label>
                      <input name="form_subject" class="form-control required" type="text" placeholder="Ingresa Asunto">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="mb-3">
                      <label>Teléfono</label>
                      <input name="form_phone" class="form-control" type="text" placeholder="Ingresa teléfono">
                    </div>
                  </div>
                </div>

                <div class="mb-3">
                  <label>Mensaje</label>
                  <textarea name="form_message" class="form-control required" rows="5" placeholder="Ingresa el mensaje"></textarea>
                </div>
                <div class="mb-3">
                  <input name="form_botcheck" class="form-control" type="hidden" value="">
                  <button type="submit" class="btn btn-flat btn-theme-colored1 text-uppercase mt-10 mb-sm-30 border-left-theme-color-2-4px" data-loading-text="Please wait...">Enviar</button>
                  <button type="reset" class="btn btn-flat btn-theme-colored3 text-uppercase mt-10 mb-sm-30 border-left-theme-color-2-4px">Limpiar</button>
                </div>
              </form>

              <!-- Contact Form Validation-->
              <script>
                (function($) {
                  $("#contact_form").validate({
                    submitHandler: function(form) {
                      var form_btn = $(form).find('button[type="submit"]');
                      var form_result_div = '#form-result';
                      $(form_result_div).remove();
                      form_btn.before('<div id="form-result" class="alert alert-success" role="alert" style="display: none;"></div>');
                      var form_btn_old_msg = form_btn.html();
                      form_btn.html(form_btn.prop('disabled', true).data("loading-text"));
                      $(form).ajaxSubmit({
                        dataType:  'json',
                        success: function(data) {
                          if( data.status == 'true' ) {
                            $(form).find('.form-control').val('');
                          }
                          form_btn.prop('disabled', false).html(form_btn_old_msg);
                          $(form_result_div).html(data.message).fadeIn('slow');
                          setTimeout(function(){ $(form_result_div).fadeOut('slow') }, 6000);
                        }
                      });
                    }
                  });
                })(jQuery);
              </script>
            </div>
          </div>
        </div>
      </div>
    </section>


    <!-- -->
    <section>
      <div class="container-fluid p-0">
        <div class="row">
          <div class="col-md-12">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d44703.28215593504!2d-70.86226596764583!3d-33.61590661728926!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9662e128b906b8a9%3A0x99b8569a3f018ad9!2sFundicionlas%20Rosas!5e0!3m2!1sen!2scl!4v1618352919167!5m2!1sen!2scl"  data-tm-width="100%" allowfullscreen="" style="width: 100%;" height="600"></iframe>
          </div>
        </div>
      </div>
    </section>


  </div>

<?php include_once("./footer.php") ?>
<a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
</div>
<?php include_once("./foot.php") ?>

